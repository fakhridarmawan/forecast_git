﻿Imports System.Data
Imports System.Data.SqlClient
    Public Class FormDataPemakaian

    'INPUT DATA JENIS

    Private Function Cekint()
        Dim I As Integer
        Try
            I = Txt_ID.Text
            Cekint = True
        Catch ex As Exception
            Cekint = False
        End Try
    End Function

    Private Function CekID(ByVal IDPemakaian As String) As Boolean

        Dim SQL As String = "SELECT COUNT(IDPemakaian) FROM Jenis WHERE IDPemakaian ='" & Txt_ID.Text & "'"
        Dim cmd As New SqlCommand(SQL, SqlConnection1)
        Dim dr As SqlDataReader
        Dim nilai As Boolean = False

        Try
            SqlConnection1.Open()
            dr = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            While dr.Read
                If dr(0) > 0 Then
                    nilai = True
                End If
            End While
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Data Sudah Ada!!")
        Finally
            SqlConnection1.Close()
        End Try
        Return nilai
    End Function

    Private Sub InputData()
        Dim SQL As String
        SqlConnection1.Open()
        SQL = "INSERT INTO Jenis VALUES ('" & Txt_ID.Text & "','" & Txt_Jenis.Text & "', '" & Txt_JenisPelanggan.Text & "')"
        SqlDataAdapter1.SelectCommand.CommandText = SQL
        SqlDataAdapter1.SelectCommand.ExecuteNonQuery()
        MessageBox.Show("Data Berhasil Anda Simpan", "Konfirmasi Berhasil", MessageBoxButtons.OK, MessageBoxIcon.Information)
        SqlConnection1.Close()
    End Sub

    Private Sub Btn_Simpan1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Simpan1.Click
        If Txt_ID.Text.Trim = "" Then
            MessageBox.Show("Masukkan ID!!", "ID!", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Txt_ID.Focus()
            Txt_ID.Text = ""
            Txt_Jenis.Text = ""
            Txt_JenisPelanggan.Text = ""
            Exit Sub
        End If
        If Cekint() = False Then
            MessageBox.Show("ID harus numerik", "ID!", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Txt_ID.Text = ""
            Txt_Jenis.Text = ""
            Txt_ID.Focus()
            Exit Sub
        End If
        If CekID(Txt_ID.Text) = False Then
            InputData()
            Txt_ID.Text = ""
            Txt_Jenis.Text = ""
            Txt_ID.Focus()
        Else
            MsgBox("Data Already Exists", MsgBoxStyle.Information, "Perhatian")
            Txt_ID.Text = ""
            Txt_Jenis.Text = ""
            Txt_JenisPelanggan.Text = ""
            Txt_ID.Focus()
            Exit Sub
        End If


        Txt_ID.Text = ""
        Txt_Jenis.Text = ""
        Txt_JenisPelanggan.Text = ""
        Txt_ID.Focus()

    End Sub

    'REFRESH DATA JENIS

    Private Sub Btn_Refresh1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Refresh1.Click
        Dim Dt As New DataTable
        Dim Dv As New DataView
        Dim SQL As String
        SQL = "SELECT IDPemakaian AS [ID Pemakaian], JenisData AS [Jenis Data per Bulan], JenisPelanggan AS [Jenis Data per Pelanggan] FROM Jenis"
        SqlDataAdapter1.SelectCommand.CommandText = SQL
        DsJenis1.Clear()
        SqlDataAdapter1.Fill(DsJenis1, "Jenis")
        Dt = DsJenis1.Tables("Jenis")
        Dv = Dt.DefaultView
        DataGridView1.DataSource = Dv
    End Sub

    'UBAH DATA JENIS

    Private Sub Btn_Ubah1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Ubah1.Click
        SqlDataAdapter1.Update(DsJenis1)
        MessageBox.Show("Data Berhasil Anda Ubah", "Konfirmasi Berhasil", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    'CARI DATA JENIS

    Private Sub CariRecord()

        If Txt_Cari.Text = "" Then
            MessageBox.Show("Isikan Kata kunci pencarian", "Cari Data", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Txt_Cari.Focus()
        End If

        Dim SQL As String

        SQL = "SELECT IDPemakaian AS [ID Pemakaian], JenisData AS [Jenis Data per Bulan], JenisPelanggan AS [Jenis Data per Pelanggan] FROM Jenis WHERE JenisData LIKE '%" & Txt_Cari.Text & "%'"

        Dim Dt As New DataTable
        Dim Dv As New DataView

        SqlDataAdapter1.SelectCommand.CommandText = SQL
        DsJenis1.Clear()
        SqlDataAdapter1.Fill(DsJenis1, "Jenis")
        Dt = DsJenis1.Tables("Jenis")

        If Dt.Rows.Count = Nothing Then
            MsgBox("Data Tidak Dapat Ditemukan", MsgBoxStyle.Information, "Perhatian")
            Txt_Cari.Clear()
            Exit Sub
        End If

        Dv = Dt.DefaultView
        DataGridView1.DataSource = Dv
        Txt_Cari.Clear()
        Txt_Cari.Focus()
        DataGridView1.Visible = True

    End Sub

    Private Sub Btn_Cari1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Cari1.Click
        Call CariRecord()
    End Sub

    'DELETE DATA JENIS

    Private Sub Btn_Delete1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Delete1.Click

        DsJenis1.Jenis.Rows(DataGridView1.CurrentRow.Index).Delete()
        SqlDataAdapter1.Update(DsJenis1.Jenis)

    End Sub

    'INPUT DATA PERIODE

    Private Function Cekint2()
        Dim I As Integer
        Try
            I = Txt_No.Text
            Cekint2 = True
        Catch ex As Exception
            Cekint2 = False
        End Try
    End Function

    Private Function CekNo(ByVal Nomor As String) As Boolean

        Dim SQL As String = "SELECT COUNT(Nomor) FROM Periode WHERE Nomor ='" & Txt_No.Text & "'"
        Dim cmd As New SqlCommand(SQL, SqlConnection1)
        Dim dr As SqlDataReader
        Dim nilai As Boolean = False

        Try
            SqlConnection1.Open()
            dr = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            While dr.Read
                If dr(0) > 0 Then
                    nilai = True
                End If
            End While
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Data Sudah Ada!!")
        Finally
            SqlConnection1.Close()
        End Try
        Return nilai
    End Function

    Private Sub InputData2()
        Dim SQL As String
        Txt_Jumlah.Text = Format("{0:##,###,##0}", CDbl(Txt_Jumlah.Text))
        SqlConnection1.Open()
        SQL = "INSERT INTO Periode VALUES ('" & Txt_No.Text & "', '" & Cbo_ID.SelectedValue & "', '" & Cbo_Jenis.SelectedValue & "', '" & Cbo_Periode.SelectedItem & "', '" & Cbo_Tahun.SelectedItem & "', '" & Txt_Jumlah.Text & "')"
        SqlDataAdapter2.SelectCommand.CommandText = SQL
        SqlDataAdapter2.SelectCommand.ExecuteNonQuery()
        MessageBox.Show("Data Berhasil Anda Simpan", "Konfirmasi Berhasil", MessageBoxButtons.OK, MessageBoxIcon.Information)
        SqlConnection1.Close()
    End Sub

    Private Sub Btn_Simpan2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Simpan2.Click
        If Txt_No.Text.Trim = "" Then
            MessageBox.Show("Masukkan ID!!", "ID!", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Txt_No.Focus()
            Txt_No.Text = ""
            Exit Sub
        End If
        If Cekint2() = False Then
            MessageBox.Show(" Data Berupa Angka!! ", "STOP!", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Txt_No.Text = ""
            Txt_Jumlah.Text = ""
            Txt_No.Focus()
            Exit Sub
        End If
        If CekNo(Txt_No.Text) = False Then
            InputData2()
            Txt_No.Text = ""
            Txt_No.Focus()
        Else
            MsgBox("Data Already Exists", MsgBoxStyle.Information, "Perhatian")
            Txt_No.Text = ""
            Txt_No.Focus()
            Exit Sub
        End If


        Txt_No.Text = ""
        Cbo_Periode.Text = "Januari"
        Txt_Jumlah.Text = ""
        Cbo_Tahun.Text = "2002"
        Txt_No.Focus()
        Txt_No.ReadOnly = False
        Cbo_Jenis.Focus()

    End Sub

    'REFRESH DATA PERIODE

    Private Sub Btn_Refresh2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Refresh2.Click
        Dim Dt As New DataTable
        Dim Dv As New DataView
        Dim SQL As String
        SQL = "SELECT Nomor, IDPemakaian AS [ID Pemakaian], JenisData AS [Jenis Data per Bulan], Periode, Tahun, Jumlah FROM Periode"
        SqlDataAdapter2.SelectCommand.CommandText = SQL
        DsPeriode1.Clear()
        SqlDataAdapter2.Fill(DsPeriode1, "Periode")
        Dt = DsPeriode1.Tables("Periode")
        Dv = Dt.DefaultView

    End Sub

    'UBAH DATA PERIODE

    Private Sub Btn_Ubah2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Ubah2.Click

        SqlDataAdapter2.Update(DsPeriode1)
        MessageBox.Show("Data Berhasil Anda Ubah", "Konfirmasi Berhasil", MessageBoxButtons.OK, MessageBoxIcon.Information)

    End Sub

    'CARI DATA PERIODE

    Private Sub CariRecord2()

        If Txt_Cari2.Text = "" Then
            MessageBox.Show("Isikan Kata kunci pencarian", "Cari Data", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Txt_Cari2.Focus()
        End If

        Dim SQL As String

        SQL = "SELECT Nomor, IDPemakaian AS [ID Pemakaian], JenisData AS [Jenis Data per Bulan], Periode, Tahun, Jumlah FROM Periode WHERE JenisData LIKE '%" & Txt_Cari2.Text & "%'"

        Dim Dt As New DataTable
        Dim Dv As New DataView

        SqlDataAdapter2.SelectCommand.CommandText = SQL
        DsPeriode1.Clear()
        SqlDataAdapter2.Fill(DsPeriode1, "Periode")
        Dt = DsPeriode1.Tables("Periode")

        If Dt.Rows.Count = Nothing Then
            MsgBox("Data Tidak Dapat Ditemukan", MsgBoxStyle.Information, "Perhatian")
            Txt_Cari2.Clear()
            Exit Sub
        End If

        Dv = Dt.DefaultView
        Txt_Cari2.Clear()
        Txt_Cari2.Focus()
        DataGridView2.Visible = True

    End Sub

    Private Sub Btn_Cari2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Cari2.Click
        Call CariRecord2()
    End Sub

    'DELETE DATA PERIODE

    Private Sub Btn_Delete2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Delete2.Click

        DsPeriode1.Periode.Rows(DataGridView2.CurrentRow.Index).Delete()
        SqlDataAdapter2.Update(DsPeriode1.Periode)

    End Sub

    'INPUT DATA PELANGGAN

    Private Function Cekint3()
        Dim I As Integer
        Try
            I = Txt_No2.Text
            Cekint3 = True
        Catch ex As Exception
            Cekint3 = False
        End Try
    End Function

    Private Function CekNo2(ByVal Nomor As String) As Boolean

        Dim SQL As String = "SELECT COUNT(Nomor) FROM Pelanggan WHERE Nomor ='" & Txt_No2.Text & "'"
        Dim cmd As New SqlCommand(SQL, SqlConnection1)
        Dim dr As SqlDataReader
        Dim nilai As Boolean = False

        Try
            SqlConnection1.Open()
            dr = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            While dr.Read
                If dr(0) > 0 Then
                    nilai = True
                End If
            End While
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Data Sudah Ada!!")
        Finally
            SqlConnection1.Close()
        End Try
        Return nilai
    End Function

    Private Sub InputData3()
        Dim SQL As String
        Txt_Jumlah2.Text = Format("{0:##,###,##0}", CDbl(Txt_Jumlah2.Text))
        SqlConnection1.Open()
        SQL = "INSERT INTO Pelanggan VALUES ('" & Txt_No2.Text & "','" & Cbo_ID2.SelectedValue & "' ,'" & Cbo_Jenis2.SelectedValue & "', '" & Cbo_Kelompok.SelectedItem & "', '" & Cbo_Tahun2.SelectedItem & "', '" & Txt_Jumlah2.Text & "')"
        SqlDataAdapter3.SelectCommand.CommandText = SQL
        SqlDataAdapter3.SelectCommand.ExecuteNonQuery()
        MessageBox.Show("Data Berhasil Anda Simpan", "Konfirmasi Berhasil", MessageBoxButtons.OK, MessageBoxIcon.Information)
        SqlConnection1.Close()
    End Sub

    Private Sub Btn_Simpan3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Simpan3.Click
        If Txt_No2.Text.Trim = "" Then
            MessageBox.Show("Masukkan ID!!", "ID!", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Txt_No2.Focus()
            Txt_No2.Text = ""
            Exit Sub
        End If
        If Cekint3() = False Then
            MessageBox.Show(" Data Berupa Angka!! ", "STOP!", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Txt_No2.Text = ""
            Txt_Jumlah2.Text = ""
            Txt_No2.Focus()
            Exit Sub
        End If
        If CekNo2(Txt_No2.Text) = False Then
            InputData3()
            Txt_No2.Text = ""
            Txt_No2.Focus()
        Else
            MsgBox("Data Already Exists", MsgBoxStyle.Information, "Perhatian")
            Txt_No2.Text = ""
            Txt_No2.Focus()
            Exit Sub
        End If

        Txt_No2.Text = ""
        Cbo_Kelompok.Text = "Rumah Tangga"
        Txt_Jumlah2.Text = ""
        Cbo_Tahun2.Text = "2002"
        Txt_No2.Focus()
        Txt_No2.ReadOnly = False
        Cbo_Jenis2.Focus()
    End Sub

    'REFRESH DATA PELANGGAN

    Private Sub Btn_Refresh3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Refresh3.Click

        Dim Dt As New DataTable
        Dim Dv As New DataView
        Dim SQL As String
        SQL = "SELECT Nomor, IDPemakaian AS [ID Pemakaian], JenisPelanggan AS [Jenis Data per Pelanggan], KelompokPelanggan AS [Kelompok Pelanggan],Tahun ,Jumlah  FROM Pelanggan "
        SqlDataAdapter3.SelectCommand.CommandText = SQL
        DsPelanggan1.Clear()
        SqlDataAdapter3.Fill(DsPelanggan1, "Pelanggan")
        Dt = DsPelanggan1.Tables("Pelanggan")
        Dv = Dt.DefaultView

    End Sub

    'UBAH DATA PELANGGAN

    Private Sub Btn_Ubah3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Ubah3.Click

        SqlDataAdapter3.Update(DsPelanggan1)
        MessageBox.Show("Data Berhasil Anda Ubah", "Konfirmasi Berhasil", MessageBoxButtons.OK, MessageBoxIcon.Information)

    End Sub

    'CARI DATA PELANGGAN

    Private Sub CariRecord3()

        If Txt_Cari3.Text = "" Then
            MessageBox.Show("Isikan Kata kunci pencarian", "Cari Data", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Txt_Cari3.Focus()
        End If

        Dim SQL As String

        SQL = "SELECT  Nomor, IDPemakaian AS [ID Pemakaian], JenisPelanggan AS [Jenis Data per Pelanggan], KelompokPelanggan AS [Kelompok Pelanggan], Tahun, Jumlah FROM Pelanggan WHERE JenisPelanggan LIKE '%" & Txt_Cari3.Text & "%'"

        Dim Dt As New DataTable
        Dim Dv As New DataView

        SqlDataAdapter3.SelectCommand.CommandText = SQL
        DsPelanggan1.Clear()
        SqlDataAdapter3.Fill(DsPelanggan1, "Pelanggan")
        Dt = DsPelanggan1.Tables("Pelanggan")

        If Dt.Rows.Count = Nothing Then
            MsgBox("Data Tidak Dapat Ditemukan", MsgBoxStyle.Information, "Perhatian")
            Txt_Cari3.Clear()
            Exit Sub
        End If

        Dv = Dt.DefaultView
        Txt_Cari3.Clear()
        Txt_Cari3.Focus()
        DataGridView3.Visible = True
        DataGridView3.DataSource = DsPelanggan1.Tables(0)
    End Sub

    Private Sub Btn_Cari3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Cari3.Click
        Call CariRecord3()
    End Sub

    'DELETE DATA PELANGGAN

    Private Sub Btn_Delete3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Delete3.Click
        DsPelanggan1.Pelanggan.Rows(DataGridView3.CurrentRow.Index).Delete()
        SqlDataAdapter3.Update(DsPelanggan1.Pelanggan)
    End Sub

    Private Sub Btn_Menu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Menu.Click
        FormMenuUtama.Show()
        Me.Hide()
    End Sub

    Private Sub Btn_Peramalan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Peramalan.Click
        FormARIMA.Show()
        Me.Hide()
    End Sub

    Private Sub Btn_Exit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Exit.Click
        Me.Close()
    End Sub

    Private Sub FormDataPemakaian_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        SqlDataAdapter1.Fill(DsJenis1)
        SqlDataAdapter2.Fill(DsPeriode1)
        SqlDataAdapter3.Fill(DsPelanggan1)
    End Sub

End Class