﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormMenuUtama
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Btn_Exit2 = New System.Windows.Forms.Button()
        Me.Btn_Peramalan2 = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Btn_Data = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Btn_Exit2
        '
        Me.Btn_Exit2.Location = New System.Drawing.Point(164, 345)
        Me.Btn_Exit2.Name = "Btn_Exit2"
        Me.Btn_Exit2.Size = New System.Drawing.Size(85, 33)
        Me.Btn_Exit2.TabIndex = 10
        Me.Btn_Exit2.Text = "EXIT"
        Me.Btn_Exit2.UseVisualStyleBackColor = True
        '
        'Btn_Peramalan2
        '
        Me.Btn_Peramalan2.Location = New System.Drawing.Point(114, 230)
        Me.Btn_Peramalan2.Name = "Btn_Peramalan2"
        Me.Btn_Peramalan2.Size = New System.Drawing.Size(188, 59)
        Me.Btn_Peramalan2.TabIndex = 9
        Me.Btn_Peramalan2.Text = "Peramalan ARIMA Monte Carlo"
        Me.Btn_Peramalan2.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(107, 43)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(207, 13)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "APLIKASI PEMAKAIAN ENERGI LISTRIK"
        '
        'Btn_Data
        '
        Me.Btn_Data.Location = New System.Drawing.Point(114, 122)
        Me.Btn_Data.Name = "Btn_Data"
        Me.Btn_Data.Size = New System.Drawing.Size(188, 59)
        Me.Btn_Data.TabIndex = 7
        Me.Btn_Data.Text = "Data Pemakaian Energi Listrik"
        Me.Btn_Data.UseVisualStyleBackColor = True
        '
        'FormMenuUtama
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(421, 421)
        Me.Controls.Add(Me.Btn_Exit2)
        Me.Controls.Add(Me.Btn_Peramalan2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Btn_Data)
        Me.Name = "FormMenuUtama"
        Me.Text = "Aplikasi Peramalan Pemakaian Energi Listrik"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Btn_Exit2 As System.Windows.Forms.Button
    Friend WithEvents Btn_Peramalan2 As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Btn_Data As System.Windows.Forms.Button

End Class
