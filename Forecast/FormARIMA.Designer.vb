﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormARIMA
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim ChartArea1 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
        Dim Legend1 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
        Dim Series1 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Dim ChartArea2 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
        Dim Legend2 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
        Dim Series2 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Me.Btn_Data2 = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Rb_Kelompok = New System.Windows.Forms.RadioButton()
        Me.Rb_Pelanggan = New System.Windows.Forms.RadioButton()
        Me.Rb_Periode = New System.Windows.Forms.RadioButton()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Cb_Pelanggan = New System.Windows.Forms.ComboBox()
        Me.Cb_Kelompok = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Btn_Submit = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Cb_Periode = New System.Windows.Forms.ComboBox()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Btn_Exit3 = New System.Windows.Forms.Button()
        Me.Btn_Menu3 = New System.Windows.Forms.Button()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.Chart1 = New System.Windows.Forms.DataVisualization.Charting.Chart()
        Me.DataGridView2 = New System.Windows.Forms.DataGridView()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.Chart2 = New System.Windows.Forms.DataVisualization.Charting.Chart()
        Me.DataGridView3 = New System.Windows.Forms.DataGridView()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.TabPage6 = New System.Windows.Forms.TabPage()
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection()
        Me.GroupBox1.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        CType(Me.Chart1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage3.SuspendLayout()
        CType(Me.Chart2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Btn_Data2
        '
        Me.Btn_Data2.Location = New System.Drawing.Point(324, 596)
        Me.Btn_Data2.Name = "Btn_Data2"
        Me.Btn_Data2.Size = New System.Drawing.Size(182, 23)
        Me.Btn_Data2.TabIndex = 30
        Me.Btn_Data2.Text = "Data Pemakaian Energi Listrik"
        Me.Btn_Data2.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Rb_Kelompok)
        Me.GroupBox1.Controls.Add(Me.Rb_Pelanggan)
        Me.GroupBox1.Controls.Add(Me.Rb_Periode)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.Cb_Pelanggan)
        Me.GroupBox1.Controls.Add(Me.Cb_Kelompok)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Btn_Submit)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Cb_Periode)
        Me.GroupBox1.Location = New System.Drawing.Point(16, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(515, 149)
        Me.GroupBox1.TabIndex = 29
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "PERAMALAN ARIMA"
        '
        'Rb_Kelompok
        '
        Me.Rb_Kelompok.AutoSize = True
        Me.Rb_Kelompok.Location = New System.Drawing.Point(350, 122)
        Me.Rb_Kelompok.Name = "Rb_Kelompok"
        Me.Rb_Kelompok.Size = New System.Drawing.Size(126, 17)
        Me.Rb_Kelompok.TabIndex = 33
        Me.Rb_Kelompok.TabStop = True
        Me.Rb_Kelompok.Text = "Kelompok Pelanggan"
        Me.Rb_Kelompok.UseVisualStyleBackColor = True
        '
        'Rb_Pelanggan
        '
        Me.Rb_Pelanggan.AutoSize = True
        Me.Rb_Pelanggan.Location = New System.Drawing.Point(189, 122)
        Me.Rb_Pelanggan.Name = "Rb_Pelanggan"
        Me.Rb_Pelanggan.Size = New System.Drawing.Size(129, 17)
        Me.Rb_Pelanggan.TabIndex = 32
        Me.Rb_Pelanggan.TabStop = True
        Me.Rb_Pelanggan.Text = "Jenis Data Pelanggan"
        Me.Rb_Pelanggan.UseVisualStyleBackColor = True
        '
        'Rb_Periode
        '
        Me.Rb_Periode.AutoSize = True
        Me.Rb_Periode.Location = New System.Drawing.Point(47, 122)
        Me.Rb_Periode.Name = "Rb_Periode"
        Me.Rb_Periode.Size = New System.Drawing.Size(114, 17)
        Me.Rb_Periode.TabIndex = 31
        Me.Rb_Periode.TabStop = True
        Me.Rb_Periode.Text = "Jenis Data Periode"
        Me.Rb_Periode.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 61)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(84, 13)
        Me.Label1.TabIndex = 30
        Me.Label1.Text = "Data Pelanggan"
        '
        'Cb_Pelanggan
        '
        Me.Cb_Pelanggan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Cb_Pelanggan.FormattingEnabled = True
        Me.Cb_Pelanggan.Location = New System.Drawing.Point(129, 56)
        Me.Cb_Pelanggan.Name = "Cb_Pelanggan"
        Me.Cb_Pelanggan.Size = New System.Drawing.Size(263, 21)
        Me.Cb_Pelanggan.TabIndex = 29
        '
        'Cb_Kelompok
        '
        Me.Cb_Kelompok.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Cb_Kelompok.FormattingEnabled = True
        Me.Cb_Kelompok.Location = New System.Drawing.Point(129, 91)
        Me.Cb_Kelompok.Name = "Cb_Kelompok"
        Me.Cb_Kelompok.Size = New System.Drawing.Size(199, 21)
        Me.Cb_Kelompok.TabIndex = 28
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(6, 94)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(108, 13)
        Me.Label4.TabIndex = 27
        Me.Label4.Text = "Kelompok Pelanggan"
        '
        'Btn_Submit
        '
        Me.Btn_Submit.Location = New System.Drawing.Point(415, 61)
        Me.Btn_Submit.Name = "Btn_Submit"
        Me.Btn_Submit.Size = New System.Drawing.Size(75, 23)
        Me.Btn_Submit.TabIndex = 24
        Me.Btn_Submit.Text = "Submit"
        Me.Btn_Submit.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 28)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(69, 13)
        Me.Label2.TabIndex = 26
        Me.Label2.Text = "Data Periode"
        '
        'Cb_Periode
        '
        Me.Cb_Periode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Cb_Periode.FormattingEnabled = True
        Me.Cb_Periode.Location = New System.Drawing.Point(129, 25)
        Me.Cb_Periode.Name = "Cb_Periode"
        Me.Cb_Periode.Size = New System.Drawing.Size(189, 21)
        Me.Cb_Periode.TabIndex = 23
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(531, 596)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(117, 23)
        Me.Button5.TabIndex = 28
        Me.Button5.Text = "Simulasi Monte Carlo"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Btn_Exit3
        '
        Me.Btn_Exit3.Location = New System.Drawing.Point(679, 596)
        Me.Btn_Exit3.Name = "Btn_Exit3"
        Me.Btn_Exit3.Size = New System.Drawing.Size(117, 23)
        Me.Btn_Exit3.TabIndex = 27
        Me.Btn_Exit3.Text = "Exit"
        Me.Btn_Exit3.UseVisualStyleBackColor = True
        '
        'Btn_Menu3
        '
        Me.Btn_Menu3.Location = New System.Drawing.Point(180, 596)
        Me.Btn_Menu3.Name = "Btn_Menu3"
        Me.Btn_Menu3.Size = New System.Drawing.Size(117, 23)
        Me.Btn_Menu3.TabIndex = 26
        Me.Btn_Menu3.Text = "Menu Utama"
        Me.Btn_Menu3.UseVisualStyleBackColor = True
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Controls.Add(Me.TabPage5)
        Me.TabControl1.Controls.Add(Me.TabPage6)
        Me.TabControl1.Location = New System.Drawing.Point(12, 167)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(969, 412)
        Me.TabControl1.TabIndex = 25
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.DataGridView1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(961, 386)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Data"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(18, 24)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.DataGridView1.RowsDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView1.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.DataGridView1.Size = New System.Drawing.Size(421, 341)
        Me.DataGridView1.TabIndex = 0
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.Chart1)
        Me.TabPage2.Controls.Add(Me.DataGridView2)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(961, 386)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Autokorelasi"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'Chart1
        '
        ChartArea1.Name = "ChartArea1"
        Me.Chart1.ChartAreas.Add(ChartArea1)
        Legend1.Name = "Legend1"
        Me.Chart1.Legends.Add(Legend1)
        Me.Chart1.Location = New System.Drawing.Point(510, 29)
        Me.Chart1.Name = "Chart1"
        Series1.ChartArea = "ChartArea1"
        Series1.Legend = "Legend1"
        Series1.Name = "Series1"
        Me.Chart1.Series.Add(Series1)
        Me.Chart1.Size = New System.Drawing.Size(376, 300)
        Me.Chart1.TabIndex = 1
        Me.Chart1.Text = "Chart1"
        '
        'DataGridView2
        '
        Me.DataGridView2.AllowUserToAddRows = False
        Me.DataGridView2.AllowUserToDeleteRows = False
        Me.DataGridView2.AllowUserToOrderColumns = True
        Me.DataGridView2.AllowUserToResizeColumns = False
        Me.DataGridView2.AllowUserToResizeRows = False
        Me.DataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView2.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.DataGridView2.Location = New System.Drawing.Point(21, 29)
        Me.DataGridView2.Name = "DataGridView2"
        Me.DataGridView2.ReadOnly = True
        Me.DataGridView2.Size = New System.Drawing.Size(336, 327)
        Me.DataGridView2.TabIndex = 0
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.Chart2)
        Me.TabPage3.Controls.Add(Me.DataGridView3)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(961, 386)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Partial Autokorelasi"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'Chart2
        '
        ChartArea2.Name = "ChartArea1"
        Me.Chart2.ChartAreas.Add(ChartArea2)
        Legend2.Name = "Legend1"
        Me.Chart2.Legends.Add(Legend2)
        Me.Chart2.Location = New System.Drawing.Point(535, 18)
        Me.Chart2.Name = "Chart2"
        Series2.ChartArea = "ChartArea1"
        Series2.Legend = "Legend1"
        Series2.Name = "Series1"
        Me.Chart2.Series.Add(Series2)
        Me.Chart2.Size = New System.Drawing.Size(396, 300)
        Me.Chart2.TabIndex = 1
        Me.Chart2.Text = "Chart2"
        '
        'DataGridView3
        '
        Me.DataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView3.Location = New System.Drawing.Point(25, 18)
        Me.DataGridView3.Name = "DataGridView3"
        Me.DataGridView3.Size = New System.Drawing.Size(451, 346)
        Me.DataGridView3.TabIndex = 0
        '
        'TabPage4
        '
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage4.Size = New System.Drawing.Size(961, 386)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "Model"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'TabPage5
        '
        Me.TabPage5.Location = New System.Drawing.Point(4, 22)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage5.Size = New System.Drawing.Size(961, 386)
        Me.TabPage5.TabIndex = 4
        Me.TabPage5.Text = "Perhitungan"
        Me.TabPage5.UseVisualStyleBackColor = True
        '
        'TabPage6
        '
        Me.TabPage6.Location = New System.Drawing.Point(4, 22)
        Me.TabPage6.Name = "TabPage6"
        Me.TabPage6.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage6.Size = New System.Drawing.Size(961, 386)
        Me.TabPage6.TabIndex = 5
        Me.TabPage6.Text = "Peramalan"
        Me.TabPage6.UseVisualStyleBackColor = True
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "Data Source=(LocalDB)\v11.0;AttachDbFilename=""C:\Users\fakhri\Documents\Visual St" & _
    "udio 2012\Projects\Forecast\Forecasting.mdf"";Integrated Security=True;Connect Ti" & _
    "meout=30"
        Me.SqlConnection1.FireInfoMessageEventOnUserErrors = False
        '
        'FormARIMA
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(993, 582)
        Me.Controls.Add(Me.Btn_Data2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Btn_Exit3)
        Me.Controls.Add(Me.Btn_Menu3)
        Me.Controls.Add(Me.TabControl1)
        Me.MaximizeBox = False
        Me.Name = "FormARIMA"
        Me.Text = "Peramalan ARIMA dan Monte Carlo"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        CType(Me.Chart1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage3.ResumeLayout(False)
        CType(Me.Chart2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Btn_Data2 As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Cb_Kelompok As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Btn_Submit As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Cb_Periode As System.Windows.Forms.ComboBox
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Btn_Exit3 As System.Windows.Forms.Button
    Friend WithEvents Btn_Menu3 As System.Windows.Forms.Button
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage5 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage6 As System.Windows.Forms.TabPage
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Cb_Pelanggan As System.Windows.Forms.ComboBox
    Friend WithEvents Rb_Pelanggan As System.Windows.Forms.RadioButton
    Friend WithEvents Rb_Periode As System.Windows.Forms.RadioButton
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Rb_Kelompok As System.Windows.Forms.RadioButton
    Friend WithEvents DataGridView2 As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridView3 As System.Windows.Forms.DataGridView
    Friend WithEvents Chart1 As System.Windows.Forms.DataVisualization.Charting.Chart
    Friend WithEvents Chart2 As System.Windows.Forms.DataVisualization.Charting.Chart
End Class
