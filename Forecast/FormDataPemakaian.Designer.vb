﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormDataPemakaian
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormDataPemakaian))
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.Btn_Exit = New System.Windows.Forms.Button()
        Me.Btn_Peramalan = New System.Windows.Forms.Button()
        Me.Btn_Menu = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Txt_JenisPelanggan = New System.Windows.Forms.TextBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Txt_ID = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Btn_Simpan1 = New System.Windows.Forms.Button()
        Me.Txt_Jenis = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.IDPemakaianDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.JenisDataPerBulanDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.JenisDataPerPelangganDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DsJenis1 = New Forecast.DsJenis()
        Me.Btn_Refresh1 = New System.Windows.Forms.Button()
        Me.Btn_Delete1 = New System.Windows.Forms.Button()
        Me.Btn_Ubah1 = New System.Windows.Forms.Button()
        Me.Btn_Cari1 = New System.Windows.Forms.Button()
        Me.Txt_Cari = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Cbo_ID = New System.Windows.Forms.ComboBox()
        Me.DsPeriode1 = New Forecast.DsPeriode()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Cbo_Tahun = New System.Windows.Forms.ComboBox()
        Me.Btn_Simpan2 = New System.Windows.Forms.Button()
        Me.Cbo_Periode = New System.Windows.Forms.ComboBox()
        Me.Txt_Jumlah = New System.Windows.Forms.TextBox()
        Me.Cbo_Jenis = New System.Windows.Forms.ComboBox()
        Me.Txt_No = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.DataGridView2 = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDPemakaianDataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.JenisDataPerBulanDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Btn_Refresh2 = New System.Windows.Forms.Button()
        Me.Btn_Delete2 = New System.Windows.Forms.Button()
        Me.Btn_Ubah2 = New System.Windows.Forms.Button()
        Me.Btn_Cari2 = New System.Windows.Forms.Button()
        Me.Txt_Cari2 = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Cbo_ID2 = New System.Windows.Forms.ComboBox()
        Me.DsPelanggan1 = New Forecast.DsPelanggan()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Btn_Simpan3 = New System.Windows.Forms.Button()
        Me.Cbo_Tahun2 = New System.Windows.Forms.ComboBox()
        Me.Cbo_Kelompok = New System.Windows.Forms.ComboBox()
        Me.Txt_Jumlah2 = New System.Windows.Forms.TextBox()
        Me.Cbo_Jenis2 = New System.Windows.Forms.ComboBox()
        Me.Txt_No2 = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.TabPage6 = New System.Windows.Forms.TabPage()
        Me.DataGridView3 = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDPemakaianDataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.JenisDataPerPelangganDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Btn_Refresh3 = New System.Windows.Forms.Button()
        Me.Btn_Delete3 = New System.Windows.Forms.Button()
        Me.Btn_Ubah3 = New System.Windows.Forms.Button()
        Me.Btn_Cari3 = New System.Windows.Forms.Button()
        Me.Txt_Cari3 = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection()
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand()
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand()
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand()
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand()
        Me.SqlDataAdapter1 = New System.Data.SqlClient.SqlDataAdapter()
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand()
        Me.SqlInsertCommand2 = New System.Data.SqlClient.SqlCommand()
        Me.SqlUpdateCommand2 = New System.Data.SqlClient.SqlCommand()
        Me.SqlDeleteCommand2 = New System.Data.SqlClient.SqlCommand()
        Me.SqlDataAdapter2 = New System.Data.SqlClient.SqlDataAdapter()
        Me.SqlSelectCommand3 = New System.Data.SqlClient.SqlCommand()
        Me.SqlInsertCommand3 = New System.Data.SqlClient.SqlCommand()
        Me.SqlUpdateCommand3 = New System.Data.SqlClient.SqlCommand()
        Me.SqlDeleteCommand3 = New System.Data.SqlClient.SqlCommand()
        Me.SqlDataAdapter3 = New System.Data.SqlClient.SqlDataAdapter()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsJenis1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.DsPeriode1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage4.SuspendLayout()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage5.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.DsPelanggan1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage6.SuspendLayout()
        CType(Me.DataGridView3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Controls.Add(Me.TabPage5)
        Me.TabControl1.Controls.Add(Me.TabPage6)
        Me.TabControl1.Location = New System.Drawing.Point(12, 12)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(839, 542)
        Me.TabControl1.TabIndex = 1
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.Btn_Exit)
        Me.TabPage1.Controls.Add(Me.Btn_Peramalan)
        Me.TabPage1.Controls.Add(Me.Btn_Menu)
        Me.TabPage1.Controls.Add(Me.GroupBox1)
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(831, 516)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Input Jenis Data"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'Btn_Exit
        '
        Me.Btn_Exit.Location = New System.Drawing.Point(530, 422)
        Me.Btn_Exit.Name = "Btn_Exit"
        Me.Btn_Exit.Size = New System.Drawing.Size(165, 40)
        Me.Btn_Exit.TabIndex = 4
        Me.Btn_Exit.Text = "Exit"
        Me.Btn_Exit.UseVisualStyleBackColor = True
        '
        'Btn_Peramalan
        '
        Me.Btn_Peramalan.Location = New System.Drawing.Point(320, 422)
        Me.Btn_Peramalan.Name = "Btn_Peramalan"
        Me.Btn_Peramalan.Size = New System.Drawing.Size(165, 40)
        Me.Btn_Peramalan.TabIndex = 3
        Me.Btn_Peramalan.Text = "Peramalan ARIMA Monte Carlo"
        Me.Btn_Peramalan.UseVisualStyleBackColor = True
        '
        'Btn_Menu
        '
        Me.Btn_Menu.Location = New System.Drawing.Point(104, 422)
        Me.Btn_Menu.Name = "Btn_Menu"
        Me.Btn_Menu.Size = New System.Drawing.Size(165, 40)
        Me.Btn_Menu.TabIndex = 2
        Me.Btn_Menu.Text = "Menu Utama"
        Me.Btn_Menu.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Txt_JenisPelanggan)
        Me.GroupBox1.Controls.Add(Me.Label22)
        Me.GroupBox1.Controls.Add(Me.Txt_ID)
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.Btn_Simpan1)
        Me.GroupBox1.Controls.Add(Me.Txt_Jenis)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Location = New System.Drawing.Point(211, 139)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(395, 203)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Input Data"
        '
        'Txt_JenisPelanggan
        '
        Me.Txt_JenisPelanggan.Location = New System.Drawing.Point(168, 99)
        Me.Txt_JenisPelanggan.Name = "Txt_JenisPelanggan"
        Me.Txt_JenisPelanggan.Size = New System.Drawing.Size(204, 20)
        Me.Txt_JenisPelanggan.TabIndex = 8
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(33, 102)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(129, 13)
        Me.Label22.TabIndex = 7
        Me.Label22.Text = "Jenis Data per Pelanggan"
        '
        'Txt_ID
        '
        Me.Txt_ID.Location = New System.Drawing.Point(168, 38)
        Me.Txt_ID.Name = "Txt_ID"
        Me.Txt_ID.Size = New System.Drawing.Size(43, 20)
        Me.Txt_ID.TabIndex = 6
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(33, 41)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(74, 13)
        Me.Label15.TabIndex = 5
        Me.Label15.Text = "ID Pemakaian"
        '
        'Btn_Simpan1
        '
        Me.Btn_Simpan1.Location = New System.Drawing.Point(138, 156)
        Me.Btn_Simpan1.Name = "Btn_Simpan1"
        Me.Btn_Simpan1.Size = New System.Drawing.Size(117, 23)
        Me.Btn_Simpan1.TabIndex = 4
        Me.Btn_Simpan1.Text = "Simpan"
        Me.Btn_Simpan1.UseVisualStyleBackColor = True
        '
        'Txt_Jenis
        '
        Me.Txt_Jenis.Location = New System.Drawing.Point(168, 69)
        Me.Txt_Jenis.Name = "Txt_Jenis"
        Me.Txt_Jenis.Size = New System.Drawing.Size(204, 20)
        Me.Txt_Jenis.TabIndex = 3
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(33, 72)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(105, 13)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Jenis Data per Bulan"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(330, 64)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(176, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Input Data Pemakaian Energi Listrik"
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.DataGridView1)
        Me.TabPage2.Controls.Add(Me.Btn_Refresh1)
        Me.TabPage2.Controls.Add(Me.Btn_Delete1)
        Me.TabPage2.Controls.Add(Me.Btn_Ubah1)
        Me.TabPage2.Controls.Add(Me.Btn_Cari1)
        Me.TabPage2.Controls.Add(Me.Txt_Cari)
        Me.TabPage2.Controls.Add(Me.Label4)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(831, 516)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Edit Jenis Data"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AutoGenerateColumns = False
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IDPemakaianDataGridViewTextBoxColumn, Me.JenisDataPerBulanDataGridViewTextBoxColumn, Me.JenisDataPerPelangganDataGridViewTextBoxColumn})
        Me.DataGridView1.DataMember = "Jenis"
        Me.DataGridView1.DataSource = Me.DsJenis1
        Me.DataGridView1.Location = New System.Drawing.Point(6, 21)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(819, 415)
        Me.DataGridView1.TabIndex = 7
        '
        'IDPemakaianDataGridViewTextBoxColumn
        '
        Me.IDPemakaianDataGridViewTextBoxColumn.DataPropertyName = "ID Pemakaian"
        Me.IDPemakaianDataGridViewTextBoxColumn.HeaderText = "ID Pemakaian"
        Me.IDPemakaianDataGridViewTextBoxColumn.Name = "IDPemakaianDataGridViewTextBoxColumn"
        '
        'JenisDataPerBulanDataGridViewTextBoxColumn
        '
        Me.JenisDataPerBulanDataGridViewTextBoxColumn.DataPropertyName = "Jenis Data per Bulan"
        Me.JenisDataPerBulanDataGridViewTextBoxColumn.HeaderText = "Jenis Data per Bulan"
        Me.JenisDataPerBulanDataGridViewTextBoxColumn.Name = "JenisDataPerBulanDataGridViewTextBoxColumn"
        Me.JenisDataPerBulanDataGridViewTextBoxColumn.Width = 180
        '
        'JenisDataPerPelangganDataGridViewTextBoxColumn
        '
        Me.JenisDataPerPelangganDataGridViewTextBoxColumn.DataPropertyName = "Jenis Data per Pelanggan"
        Me.JenisDataPerPelangganDataGridViewTextBoxColumn.HeaderText = "Jenis Data per Pelanggan"
        Me.JenisDataPerPelangganDataGridViewTextBoxColumn.Name = "JenisDataPerPelangganDataGridViewTextBoxColumn"
        Me.JenisDataPerPelangganDataGridViewTextBoxColumn.Width = 250
        '
        'DsJenis1
        '
        Me.DsJenis1.DataSetName = "DsJenis"
        Me.DsJenis1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Btn_Refresh1
        '
        Me.Btn_Refresh1.Location = New System.Drawing.Point(632, 461)
        Me.Btn_Refresh1.Name = "Btn_Refresh1"
        Me.Btn_Refresh1.Size = New System.Drawing.Size(116, 25)
        Me.Btn_Refresh1.TabIndex = 6
        Me.Btn_Refresh1.Text = "Refresh"
        Me.Btn_Refresh1.UseVisualStyleBackColor = True
        '
        'Btn_Delete1
        '
        Me.Btn_Delete1.Location = New System.Drawing.Point(533, 463)
        Me.Btn_Delete1.Name = "Btn_Delete1"
        Me.Btn_Delete1.Size = New System.Drawing.Size(75, 23)
        Me.Btn_Delete1.TabIndex = 5
        Me.Btn_Delete1.Text = "Delete"
        Me.Btn_Delete1.UseVisualStyleBackColor = True
        '
        'Btn_Ubah1
        '
        Me.Btn_Ubah1.Location = New System.Drawing.Point(439, 463)
        Me.Btn_Ubah1.Name = "Btn_Ubah1"
        Me.Btn_Ubah1.Size = New System.Drawing.Size(75, 23)
        Me.Btn_Ubah1.TabIndex = 4
        Me.Btn_Ubah1.Text = "Ubah"
        Me.Btn_Ubah1.UseVisualStyleBackColor = True
        '
        'Btn_Cari1
        '
        Me.Btn_Cari1.Location = New System.Drawing.Point(345, 463)
        Me.Btn_Cari1.Name = "Btn_Cari1"
        Me.Btn_Cari1.Size = New System.Drawing.Size(75, 23)
        Me.Btn_Cari1.TabIndex = 3
        Me.Btn_Cari1.Text = "Cari"
        Me.Btn_Cari1.UseVisualStyleBackColor = True
        '
        'Txt_Cari
        '
        Me.Txt_Cari.Location = New System.Drawing.Point(101, 468)
        Me.Txt_Cari.Name = "Txt_Cari"
        Me.Txt_Cari.Size = New System.Drawing.Size(216, 20)
        Me.Txt_Cari.TabIndex = 2
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(38, 473)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(57, 13)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "Jenis Data"
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.GroupBox2)
        Me.TabPage3.Controls.Add(Me.Label5)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(831, 516)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Input Periode Data"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Cbo_ID)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.Cbo_Tahun)
        Me.GroupBox2.Controls.Add(Me.Btn_Simpan2)
        Me.GroupBox2.Controls.Add(Me.Cbo_Periode)
        Me.GroupBox2.Controls.Add(Me.Txt_Jumlah)
        Me.GroupBox2.Controls.Add(Me.Cbo_Jenis)
        Me.GroupBox2.Controls.Add(Me.Txt_No)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Controls.Add(Me.Label10)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Location = New System.Drawing.Point(231, 125)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(348, 301)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Input Data Periode"
        '
        'Cbo_ID
        '
        Me.Cbo_ID.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.DsPeriode1, "Periode.ID Pemakaian", True))
        Me.Cbo_ID.DataSource = Me.DsJenis1
        Me.Cbo_ID.DisplayMember = "Jenis.ID Pemakaian"
        Me.Cbo_ID.FormattingEnabled = True
        Me.Cbo_ID.Location = New System.Drawing.Point(130, 70)
        Me.Cbo_ID.Name = "Cbo_ID"
        Me.Cbo_ID.Size = New System.Drawing.Size(45, 21)
        Me.Cbo_ID.TabIndex = 15
        Me.Cbo_ID.ValueMember = "Jenis.ID Pemakaian"
        '
        'DsPeriode1
        '
        Me.DsPeriode1.DataSetName = "DsPeriode"
        Me.DsPeriode1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(19, 70)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(74, 13)
        Me.Label2.TabIndex = 14
        Me.Label2.Text = "ID Pemakaian"
        '
        'Cbo_Tahun
        '
        Me.Cbo_Tahun.FormattingEnabled = True
        Me.Cbo_Tahun.Items.AddRange(New Object() {"2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012"})
        Me.Cbo_Tahun.Location = New System.Drawing.Point(130, 167)
        Me.Cbo_Tahun.Name = "Cbo_Tahun"
        Me.Cbo_Tahun.Size = New System.Drawing.Size(55, 21)
        Me.Cbo_Tahun.TabIndex = 13
        '
        'Btn_Simpan2
        '
        Me.Btn_Simpan2.Location = New System.Drawing.Point(120, 253)
        Me.Btn_Simpan2.Name = "Btn_Simpan2"
        Me.Btn_Simpan2.Size = New System.Drawing.Size(109, 23)
        Me.Btn_Simpan2.TabIndex = 12
        Me.Btn_Simpan2.Text = "Simpan"
        Me.Btn_Simpan2.UseVisualStyleBackColor = True
        '
        'Cbo_Periode
        '
        Me.Cbo_Periode.FormattingEnabled = True
        Me.Cbo_Periode.Items.AddRange(New Object() {"Januari", "Pebruari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "Nopember", "Desember"})
        Me.Cbo_Periode.Location = New System.Drawing.Point(130, 133)
        Me.Cbo_Periode.Name = "Cbo_Periode"
        Me.Cbo_Periode.Size = New System.Drawing.Size(79, 21)
        Me.Cbo_Periode.TabIndex = 10
        '
        'Txt_Jumlah
        '
        Me.Txt_Jumlah.Location = New System.Drawing.Point(130, 203)
        Me.Txt_Jumlah.Name = "Txt_Jumlah"
        Me.Txt_Jumlah.Size = New System.Drawing.Size(110, 20)
        Me.Txt_Jumlah.TabIndex = 9
        '
        'Cbo_Jenis
        '
        Me.Cbo_Jenis.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.DsPeriode1, "Periode.Jenis Data per Bulan", True))
        Me.Cbo_Jenis.DataSource = Me.DsJenis1
        Me.Cbo_Jenis.DisplayMember = "Jenis.Jenis Data per Bulan"
        Me.Cbo_Jenis.FormattingEnabled = True
        Me.Cbo_Jenis.Location = New System.Drawing.Point(130, 101)
        Me.Cbo_Jenis.Name = "Cbo_Jenis"
        Me.Cbo_Jenis.Size = New System.Drawing.Size(194, 21)
        Me.Cbo_Jenis.TabIndex = 7
        Me.Cbo_Jenis.ValueMember = "Jenis.Jenis Data per Bulan"
        '
        'Txt_No
        '
        Me.Txt_No.Location = New System.Drawing.Point(130, 37)
        Me.Txt_No.Name = "Txt_No"
        Me.Txt_No.Size = New System.Drawing.Size(45, 20)
        Me.Txt_No.TabIndex = 6
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(19, 175)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(38, 13)
        Me.Label11.TabIndex = 5
        Me.Label11.Text = "Tahun"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(19, 210)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(40, 13)
        Me.Label10.TabIndex = 4
        Me.Label10.Text = "Jumlah"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(19, 141)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(43, 13)
        Me.Label9.TabIndex = 3
        Me.Label9.Text = "Periode"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(19, 109)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(105, 13)
        Me.Label8.TabIndex = 2
        Me.Label8.Text = "Jenis Data per Bulan"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(19, 37)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(38, 13)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "Nomor"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(289, 65)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(233, 13)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "Input Data Pemakaian Energi Listrik per Periode"
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.DataGridView2)
        Me.TabPage4.Controls.Add(Me.Btn_Refresh2)
        Me.TabPage4.Controls.Add(Me.Btn_Delete2)
        Me.TabPage4.Controls.Add(Me.Btn_Ubah2)
        Me.TabPage4.Controls.Add(Me.Btn_Cari2)
        Me.TabPage4.Controls.Add(Me.Txt_Cari2)
        Me.TabPage4.Controls.Add(Me.Label12)
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Size = New System.Drawing.Size(831, 516)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "Edit Periode Data"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'DataGridView2
        '
        Me.DataGridView2.AllowUserToAddRows = False
        Me.DataGridView2.AllowUserToDeleteRows = False
        Me.DataGridView2.AutoGenerateColumns = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView2.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView2.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.IDPemakaianDataGridViewTextBoxColumn4, Me.JenisDataPerBulanDataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4})
        Me.DataGridView2.DataMember = "Periode"
        Me.DataGridView2.DataSource = Me.DsPeriode1
        Me.DataGridView2.Location = New System.Drawing.Point(17, 14)
        Me.DataGridView2.Name = "DataGridView2"
        Me.DataGridView2.Size = New System.Drawing.Size(798, 429)
        Me.DataGridView2.TabIndex = 7
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "Nomor"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.DataGridViewTextBoxColumn1.DefaultCellStyle = DataGridViewCellStyle2
        Me.DataGridViewTextBoxColumn1.HeaderText = "Nomor"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.Width = 50
        '
        'IDPemakaianDataGridViewTextBoxColumn4
        '
        Me.IDPemakaianDataGridViewTextBoxColumn4.DataPropertyName = "ID Pemakaian"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.IDPemakaianDataGridViewTextBoxColumn4.DefaultCellStyle = DataGridViewCellStyle3
        Me.IDPemakaianDataGridViewTextBoxColumn4.HeaderText = "ID Pemakaian"
        Me.IDPemakaianDataGridViewTextBoxColumn4.Name = "IDPemakaianDataGridViewTextBoxColumn4"
        '
        'JenisDataPerBulanDataGridViewTextBoxColumn1
        '
        Me.JenisDataPerBulanDataGridViewTextBoxColumn1.DataPropertyName = "Jenis Data per Bulan"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.JenisDataPerBulanDataGridViewTextBoxColumn1.DefaultCellStyle = DataGridViewCellStyle4
        Me.JenisDataPerBulanDataGridViewTextBoxColumn1.HeaderText = "Jenis Data per Bulan"
        Me.JenisDataPerBulanDataGridViewTextBoxColumn1.Name = "JenisDataPerBulanDataGridViewTextBoxColumn1"
        Me.JenisDataPerBulanDataGridViewTextBoxColumn1.Width = 180
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "Periode"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.DataGridViewTextBoxColumn2.DefaultCellStyle = DataGridViewCellStyle5
        Me.DataGridViewTextBoxColumn2.HeaderText = "Periode"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.Width = 120
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "Tahun"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.DataGridViewTextBoxColumn3.DefaultCellStyle = DataGridViewCellStyle6
        Me.DataGridViewTextBoxColumn3.HeaderText = "Tahun"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.Width = 85
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "Jumlah"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle7.Format = "N2"
        DataGridViewCellStyle7.NullValue = "0.00"
        Me.DataGridViewTextBoxColumn4.DefaultCellStyle = DataGridViewCellStyle7
        Me.DataGridViewTextBoxColumn4.HeaderText = "Jumlah"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.Width = 150
        '
        'Btn_Refresh2
        '
        Me.Btn_Refresh2.Location = New System.Drawing.Point(641, 465)
        Me.Btn_Refresh2.Name = "Btn_Refresh2"
        Me.Btn_Refresh2.Size = New System.Drawing.Size(116, 25)
        Me.Btn_Refresh2.TabIndex = 6
        Me.Btn_Refresh2.Text = "Refresh"
        Me.Btn_Refresh2.UseVisualStyleBackColor = True
        '
        'Btn_Delete2
        '
        Me.Btn_Delete2.Location = New System.Drawing.Point(551, 465)
        Me.Btn_Delete2.Name = "Btn_Delete2"
        Me.Btn_Delete2.Size = New System.Drawing.Size(75, 23)
        Me.Btn_Delete2.TabIndex = 5
        Me.Btn_Delete2.Text = "Delete"
        Me.Btn_Delete2.UseVisualStyleBackColor = True
        '
        'Btn_Ubah2
        '
        Me.Btn_Ubah2.Location = New System.Drawing.Point(456, 465)
        Me.Btn_Ubah2.Name = "Btn_Ubah2"
        Me.Btn_Ubah2.Size = New System.Drawing.Size(75, 23)
        Me.Btn_Ubah2.TabIndex = 4
        Me.Btn_Ubah2.Text = "Ubah"
        Me.Btn_Ubah2.UseVisualStyleBackColor = True
        '
        'Btn_Cari2
        '
        Me.Btn_Cari2.Location = New System.Drawing.Point(360, 464)
        Me.Btn_Cari2.Name = "Btn_Cari2"
        Me.Btn_Cari2.Size = New System.Drawing.Size(75, 23)
        Me.Btn_Cari2.TabIndex = 3
        Me.Btn_Cari2.Text = "Cari"
        Me.Btn_Cari2.UseVisualStyleBackColor = True
        '
        'Txt_Cari2
        '
        Me.Txt_Cari2.Location = New System.Drawing.Point(112, 466)
        Me.Txt_Cari2.Name = "Txt_Cari2"
        Me.Txt_Cari2.Size = New System.Drawing.Size(216, 20)
        Me.Txt_Cari2.TabIndex = 2
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(40, 469)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(57, 13)
        Me.Label12.TabIndex = 1
        Me.Label12.Text = "Jenis Data"
        '
        'TabPage5
        '
        Me.TabPage5.Controls.Add(Me.GroupBox3)
        Me.TabPage5.Controls.Add(Me.Label13)
        Me.TabPage5.Location = New System.Drawing.Point(4, 22)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Size = New System.Drawing.Size(831, 516)
        Me.TabPage5.TabIndex = 4
        Me.TabPage5.Text = "Input Pelanggan Data"
        Me.TabPage5.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Label21)
        Me.GroupBox3.Controls.Add(Me.Cbo_ID2)
        Me.GroupBox3.Controls.Add(Me.Label7)
        Me.GroupBox3.Controls.Add(Me.Btn_Simpan3)
        Me.GroupBox3.Controls.Add(Me.Cbo_Tahun2)
        Me.GroupBox3.Controls.Add(Me.Cbo_Kelompok)
        Me.GroupBox3.Controls.Add(Me.Txt_Jumlah2)
        Me.GroupBox3.Controls.Add(Me.Cbo_Jenis2)
        Me.GroupBox3.Controls.Add(Me.Txt_No2)
        Me.GroupBox3.Controls.Add(Me.Label19)
        Me.GroupBox3.Controls.Add(Me.Label18)
        Me.GroupBox3.Controls.Add(Me.Label17)
        Me.GroupBox3.Controls.Add(Me.Label16)
        Me.GroupBox3.Controls.Add(Me.Label14)
        Me.GroupBox3.Location = New System.Drawing.Point(197, 126)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(438, 309)
        Me.GroupBox3.TabIndex = 1
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Input Data Pelanggan"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(18, 282)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(162, 13)
        Me.Label21.TabIndex = 17
        Me.Label21.Text = "*PJU = Penerangan Jalan Umum"
        '
        'Cbo_ID2
        '
        Me.Cbo_ID2.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.DsPelanggan1, "Pelanggan.ID Pemakaian", True))
        Me.Cbo_ID2.DataSource = Me.DsJenis1
        Me.Cbo_ID2.DisplayMember = "Jenis.ID Pemakaian"
        Me.Cbo_ID2.FormattingEnabled = True
        Me.Cbo_ID2.Location = New System.Drawing.Point(153, 65)
        Me.Cbo_ID2.Name = "Cbo_ID2"
        Me.Cbo_ID2.Size = New System.Drawing.Size(42, 21)
        Me.Cbo_ID2.TabIndex = 14
        Me.Cbo_ID2.ValueMember = "Jenis.ID Pemakaian"
        '
        'DsPelanggan1
        '
        Me.DsPelanggan1.DataSetName = "DsPelanggan"
        Me.DsPelanggan1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(18, 68)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(74, 13)
        Me.Label7.TabIndex = 13
        Me.Label7.Text = "ID Pemakaian"
        '
        'Btn_Simpan3
        '
        Me.Btn_Simpan3.Location = New System.Drawing.Point(170, 241)
        Me.Btn_Simpan3.Name = "Btn_Simpan3"
        Me.Btn_Simpan3.Size = New System.Drawing.Size(105, 23)
        Me.Btn_Simpan3.TabIndex = 12
        Me.Btn_Simpan3.Text = "Simpan"
        Me.Btn_Simpan3.UseVisualStyleBackColor = True
        '
        'Cbo_Tahun2
        '
        Me.Cbo_Tahun2.FormattingEnabled = True
        Me.Cbo_Tahun2.Items.AddRange(New Object() {"2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012"})
        Me.Cbo_Tahun2.Location = New System.Drawing.Point(153, 163)
        Me.Cbo_Tahun2.Name = "Cbo_Tahun2"
        Me.Cbo_Tahun2.Size = New System.Drawing.Size(59, 21)
        Me.Cbo_Tahun2.TabIndex = 11
        '
        'Cbo_Kelompok
        '
        Me.Cbo_Kelompok.FormattingEnabled = True
        Me.Cbo_Kelompok.Items.AddRange(New Object() {"Rumah Tangga", "Industri", "Bisnis & Multiguna", "Sosial", "Kantor Pemerintahan", "PJU"})
        Me.Cbo_Kelompok.Location = New System.Drawing.Point(153, 130)
        Me.Cbo_Kelompok.Name = "Cbo_Kelompok"
        Me.Cbo_Kelompok.Size = New System.Drawing.Size(125, 21)
        Me.Cbo_Kelompok.TabIndex = 10
        '
        'Txt_Jumlah2
        '
        Me.Txt_Jumlah2.Location = New System.Drawing.Point(153, 200)
        Me.Txt_Jumlah2.Name = "Txt_Jumlah2"
        Me.Txt_Jumlah2.Size = New System.Drawing.Size(110, 20)
        Me.Txt_Jumlah2.TabIndex = 9
        '
        'Cbo_Jenis2
        '
        Me.Cbo_Jenis2.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.DsPelanggan1, "Pelanggan.Jenis Data per Pelanggan", True))
        Me.Cbo_Jenis2.DataSource = Me.DsJenis1
        Me.Cbo_Jenis2.DisplayMember = "Jenis.Jenis Data per Pelanggan"
        Me.Cbo_Jenis2.FormattingEnabled = True
        Me.Cbo_Jenis2.Location = New System.Drawing.Point(153, 98)
        Me.Cbo_Jenis2.Name = "Cbo_Jenis2"
        Me.Cbo_Jenis2.Size = New System.Drawing.Size(266, 21)
        Me.Cbo_Jenis2.TabIndex = 7
        Me.Cbo_Jenis2.ValueMember = "Jenis.Jenis Data per Pelanggan"
        '
        'Txt_No2
        '
        Me.Txt_No2.Location = New System.Drawing.Point(153, 35)
        Me.Txt_No2.Name = "Txt_No2"
        Me.Txt_No2.Size = New System.Drawing.Size(42, 20)
        Me.Txt_No2.TabIndex = 6
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(18, 171)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(38, 13)
        Me.Label19.TabIndex = 5
        Me.Label19.Text = "Tahun"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(18, 203)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(40, 13)
        Me.Label18.TabIndex = 4
        Me.Label18.Text = "Jumlah"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(18, 138)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(108, 13)
        Me.Label17.TabIndex = 3
        Me.Label17.Text = "Kelompok Pelanggan"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(18, 101)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(129, 13)
        Me.Label16.TabIndex = 2
        Me.Label16.Text = "Jenis Data per Pelanggan"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(18, 35)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(38, 13)
        Me.Label14.TabIndex = 0
        Me.Label14.Text = "Nomor"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(259, 57)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(298, 13)
        Me.Label13.TabIndex = 0
        Me.Label13.Text = "Input Data Pemakaian Energi Listrik per Kelompok Pelanggan"
        '
        'TabPage6
        '
        Me.TabPage6.Controls.Add(Me.DataGridView3)
        Me.TabPage6.Controls.Add(Me.Btn_Refresh3)
        Me.TabPage6.Controls.Add(Me.Btn_Delete3)
        Me.TabPage6.Controls.Add(Me.Btn_Ubah3)
        Me.TabPage6.Controls.Add(Me.Btn_Cari3)
        Me.TabPage6.Controls.Add(Me.Txt_Cari3)
        Me.TabPage6.Controls.Add(Me.Label20)
        Me.TabPage6.Location = New System.Drawing.Point(4, 22)
        Me.TabPage6.Name = "TabPage6"
        Me.TabPage6.Size = New System.Drawing.Size(831, 516)
        Me.TabPage6.TabIndex = 5
        Me.TabPage6.Text = "Edit Pelanggan Data"
        Me.TabPage6.UseVisualStyleBackColor = True
        '
        'DataGridView3
        '
        Me.DataGridView3.AllowUserToAddRows = False
        Me.DataGridView3.AllowUserToDeleteRows = False
        Me.DataGridView3.AutoGenerateColumns = False
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView3.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle8
        Me.DataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView3.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn8, Me.IDPemakaianDataGridViewTextBoxColumn5, Me.JenisDataPerPelangganDataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn9, Me.DataGridViewTextBoxColumn10, Me.DataGridViewTextBoxColumn11})
        Me.DataGridView3.DataMember = "Pelanggan"
        Me.DataGridView3.DataSource = Me.DsPelanggan1
        Me.DataGridView3.Location = New System.Drawing.Point(18, 19)
        Me.DataGridView3.Name = "DataGridView3"
        Me.DataGridView3.Size = New System.Drawing.Size(796, 425)
        Me.DataGridView3.TabIndex = 7
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.DataPropertyName = "Nomor"
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle9.NullValue = Nothing
        Me.DataGridViewTextBoxColumn8.DefaultCellStyle = DataGridViewCellStyle9
        Me.DataGridViewTextBoxColumn8.HeaderText = "Nomor"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.Width = 50
        '
        'IDPemakaianDataGridViewTextBoxColumn5
        '
        Me.IDPemakaianDataGridViewTextBoxColumn5.DataPropertyName = "ID Pemakaian"
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.IDPemakaianDataGridViewTextBoxColumn5.DefaultCellStyle = DataGridViewCellStyle10
        Me.IDPemakaianDataGridViewTextBoxColumn5.HeaderText = "ID Pemakaian"
        Me.IDPemakaianDataGridViewTextBoxColumn5.Name = "IDPemakaianDataGridViewTextBoxColumn5"
        '
        'JenisDataPerPelangganDataGridViewTextBoxColumn1
        '
        Me.JenisDataPerPelangganDataGridViewTextBoxColumn1.DataPropertyName = "Jenis Data per Pelanggan"
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.JenisDataPerPelangganDataGridViewTextBoxColumn1.DefaultCellStyle = DataGridViewCellStyle11
        Me.JenisDataPerPelangganDataGridViewTextBoxColumn1.HeaderText = "Jenis Data per Pelanggan"
        Me.JenisDataPerPelangganDataGridViewTextBoxColumn1.Name = "JenisDataPerPelangganDataGridViewTextBoxColumn1"
        Me.JenisDataPerPelangganDataGridViewTextBoxColumn1.Width = 250
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.DataPropertyName = "Kelompok Pelanggan"
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.DataGridViewTextBoxColumn9.DefaultCellStyle = DataGridViewCellStyle12
        Me.DataGridViewTextBoxColumn9.HeaderText = "Kelompok Pelanggan"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.Width = 150
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.DataPropertyName = "Tahun"
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.DataGridViewTextBoxColumn10.DefaultCellStyle = DataGridViewCellStyle13
        Me.DataGridViewTextBoxColumn10.HeaderText = "Tahun"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.Width = 70
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.DataPropertyName = "Jumlah"
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle14.Format = "N2"
        DataGridViewCellStyle14.NullValue = "0.00"
        Me.DataGridViewTextBoxColumn11.DefaultCellStyle = DataGridViewCellStyle14
        Me.DataGridViewTextBoxColumn11.HeaderText = "Jumlah"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.Width = 120
        '
        'Btn_Refresh3
        '
        Me.Btn_Refresh3.Location = New System.Drawing.Point(656, 464)
        Me.Btn_Refresh3.Name = "Btn_Refresh3"
        Me.Btn_Refresh3.Size = New System.Drawing.Size(95, 23)
        Me.Btn_Refresh3.TabIndex = 6
        Me.Btn_Refresh3.Text = "Refresh"
        Me.Btn_Refresh3.UseVisualStyleBackColor = True
        '
        'Btn_Delete3
        '
        Me.Btn_Delete3.Location = New System.Drawing.Point(566, 465)
        Me.Btn_Delete3.Name = "Btn_Delete3"
        Me.Btn_Delete3.Size = New System.Drawing.Size(75, 23)
        Me.Btn_Delete3.TabIndex = 5
        Me.Btn_Delete3.Text = "Delete"
        Me.Btn_Delete3.UseVisualStyleBackColor = True
        '
        'Btn_Ubah3
        '
        Me.Btn_Ubah3.Location = New System.Drawing.Point(475, 465)
        Me.Btn_Ubah3.Name = "Btn_Ubah3"
        Me.Btn_Ubah3.Size = New System.Drawing.Size(75, 23)
        Me.Btn_Ubah3.TabIndex = 4
        Me.Btn_Ubah3.Text = "Ubah"
        Me.Btn_Ubah3.UseVisualStyleBackColor = True
        '
        'Btn_Cari3
        '
        Me.Btn_Cari3.Location = New System.Drawing.Point(385, 465)
        Me.Btn_Cari3.Name = "Btn_Cari3"
        Me.Btn_Cari3.Size = New System.Drawing.Size(75, 23)
        Me.Btn_Cari3.TabIndex = 3
        Me.Btn_Cari3.Text = "Cari"
        Me.Btn_Cari3.UseVisualStyleBackColor = True
        '
        'Txt_Cari3
        '
        Me.Txt_Cari3.Location = New System.Drawing.Point(168, 466)
        Me.Txt_Cari3.Name = "Txt_Cari3"
        Me.Txt_Cari3.Size = New System.Drawing.Size(198, 20)
        Me.Txt_Cari3.TabIndex = 2
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(78, 469)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(57, 13)
        Me.Label20.TabIndex = 1
        Me.Label20.Text = "Jenis Data"
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "Data Source=.\SQLEXPRESS;AttachDbFilename=""C:\Users\owner\Documents\Visual Studio" & _
    " 2010\Projects\Forecast\Forecasting.mdf"";Integrated Security=True;Connect Timeou" & _
    "t=30;User Instance=True"
        Me.SqlConnection1.FireInfoMessageEventOnUserErrors = False
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT        IDPemakaian AS [ID Pemakaian], JenisData AS [Jenis Data per Bulan]," & _
    " JenisPelanggan AS [Jenis Data per Pelanggan]" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "FROM            Jenis"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection1
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = resources.GetString("SqlInsertCommand1.CommandText")
        Me.SqlInsertCommand1.Connection = Me.SqlConnection1
        Me.SqlInsertCommand1.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@ID_Pemakaian", System.Data.SqlDbType.Int, 0, "ID Pemakaian"), New System.Data.SqlClient.SqlParameter("@Jenis_Data_per_Bulan", System.Data.SqlDbType.VarChar, 0, "Jenis Data per Bulan"), New System.Data.SqlClient.SqlParameter("@Jenis_Data_per_Pelanggan", System.Data.SqlDbType.VarChar, 0, "Jenis Data per Pelanggan"), New System.Data.SqlClient.SqlParameter("@IDPemakaian", System.Data.SqlDbType.Int, 4, "ID Pemakaian")})
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = resources.GetString("SqlUpdateCommand1.CommandText")
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand1.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@ID_Pemakaian", System.Data.SqlDbType.Int, 0, "ID Pemakaian"), New System.Data.SqlClient.SqlParameter("@Jenis_Data_per_Bulan", System.Data.SqlDbType.VarChar, 0, "Jenis Data per Bulan"), New System.Data.SqlClient.SqlParameter("@Jenis_Data_per_Pelanggan", System.Data.SqlDbType.VarChar, 0, "Jenis Data per Pelanggan"), New System.Data.SqlClient.SqlParameter("@Original_ID_Pemakaian", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ID Pemakaian", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@IsNull_Jenis_Data_per_Bulan", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, CType(0, Byte), CType(0, Byte), "Jenis Data per Bulan", System.Data.DataRowVersion.Original, True, Nothing, "", "", ""), New System.Data.SqlClient.SqlParameter("@Original_Jenis_Data_per_Bulan", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Jenis Data per Bulan", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@IsNull_Jenis_Data_per_Pelanggan", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, CType(0, Byte), CType(0, Byte), "Jenis Data per Pelanggan", System.Data.DataRowVersion.Original, True, Nothing, "", "", ""), New System.Data.SqlClient.SqlParameter("@Original_Jenis_Data_per_Pelanggan", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Jenis Data per Pelanggan", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@IDPemakaian", System.Data.SqlDbType.Int, 4, "ID Pemakaian")})
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = resources.GetString("SqlDeleteCommand1.CommandText")
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand1.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_ID_Pemakaian", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ID Pemakaian", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@IsNull_Jenis_Data_per_Bulan", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, CType(0, Byte), CType(0, Byte), "Jenis Data per Bulan", System.Data.DataRowVersion.Original, True, Nothing, "", "", ""), New System.Data.SqlClient.SqlParameter("@Original_Jenis_Data_per_Bulan", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Jenis Data per Bulan", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@IsNull_Jenis_Data_per_Pelanggan", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, CType(0, Byte), CType(0, Byte), "Jenis Data per Pelanggan", System.Data.DataRowVersion.Original, True, Nothing, "", "", ""), New System.Data.SqlClient.SqlParameter("@Original_Jenis_Data_per_Pelanggan", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Jenis Data per Pelanggan", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlDataAdapter1
        '
        Me.SqlDataAdapter1.DeleteCommand = Me.SqlDeleteCommand1
        Me.SqlDataAdapter1.InsertCommand = Me.SqlInsertCommand1
        Me.SqlDataAdapter1.SelectCommand = Me.SqlSelectCommand1
        Me.SqlDataAdapter1.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Jenis", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("ID Pemakaian", "ID Pemakaian"), New System.Data.Common.DataColumnMapping("Jenis Data per Bulan", "Jenis Data per Bulan"), New System.Data.Common.DataColumnMapping("Jenis Data per Pelanggan", "Jenis Data per Pelanggan")})})
        Me.SqlDataAdapter1.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlSelectCommand2
        '
        Me.SqlSelectCommand2.CommandText = "SELECT        Nomor, IDPemakaian AS [ID Pemakaian], JenisData AS [Jenis Data per " & _
    "Bulan], Periode, Tahun, Jumlah" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "FROM            Periode"
        Me.SqlSelectCommand2.Connection = Me.SqlConnection1
        '
        'SqlInsertCommand2
        '
        Me.SqlInsertCommand2.CommandText = resources.GetString("SqlInsertCommand2.CommandText")
        Me.SqlInsertCommand2.Connection = Me.SqlConnection1
        Me.SqlInsertCommand2.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Nomor", System.Data.SqlDbType.Int, 0, "Nomor"), New System.Data.SqlClient.SqlParameter("@ID_Pemakaian", System.Data.SqlDbType.Int, 0, "ID Pemakaian"), New System.Data.SqlClient.SqlParameter("@Jenis_Data_per_Bulan", System.Data.SqlDbType.VarChar, 0, "Jenis Data per Bulan"), New System.Data.SqlClient.SqlParameter("@Periode", System.Data.SqlDbType.VarChar, 0, "Periode"), New System.Data.SqlClient.SqlParameter("@Tahun", System.Data.SqlDbType.VarChar, 0, "Tahun"), New System.Data.SqlClient.SqlParameter("@Jumlah", System.Data.SqlDbType.Float, 0, "Jumlah")})
        '
        'SqlUpdateCommand2
        '
        Me.SqlUpdateCommand2.CommandText = resources.GetString("SqlUpdateCommand2.CommandText")
        Me.SqlUpdateCommand2.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand2.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Nomor", System.Data.SqlDbType.Int, 0, "Nomor"), New System.Data.SqlClient.SqlParameter("@ID_Pemakaian", System.Data.SqlDbType.Int, 0, "ID Pemakaian"), New System.Data.SqlClient.SqlParameter("@Jenis_Data_per_Bulan", System.Data.SqlDbType.VarChar, 0, "Jenis Data per Bulan"), New System.Data.SqlClient.SqlParameter("@Periode", System.Data.SqlDbType.VarChar, 0, "Periode"), New System.Data.SqlClient.SqlParameter("@Tahun", System.Data.SqlDbType.VarChar, 0, "Tahun"), New System.Data.SqlClient.SqlParameter("@Jumlah", System.Data.SqlDbType.Float, 0, "Jumlah"), New System.Data.SqlClient.SqlParameter("@Original_Nomor", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nomor", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@IsNull_ID_Pemakaian", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, CType(0, Byte), CType(0, Byte), "ID Pemakaian", System.Data.DataRowVersion.Original, True, Nothing, "", "", ""), New System.Data.SqlClient.SqlParameter("@Original_ID_Pemakaian", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ID Pemakaian", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@IsNull_Jenis_Data_per_Bulan", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, CType(0, Byte), CType(0, Byte), "Jenis Data per Bulan", System.Data.DataRowVersion.Original, True, Nothing, "", "", ""), New System.Data.SqlClient.SqlParameter("@Original_Jenis_Data_per_Bulan", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Jenis Data per Bulan", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@IsNull_Periode", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, CType(0, Byte), CType(0, Byte), "Periode", System.Data.DataRowVersion.Original, True, Nothing, "", "", ""), New System.Data.SqlClient.SqlParameter("@Original_Periode", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Periode", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@IsNull_Tahun", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, CType(0, Byte), CType(0, Byte), "Tahun", System.Data.DataRowVersion.Original, True, Nothing, "", "", ""), New System.Data.SqlClient.SqlParameter("@Original_Tahun", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Tahun", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@IsNull_Jumlah", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, CType(0, Byte), CType(0, Byte), "Jumlah", System.Data.DataRowVersion.Original, True, Nothing, "", "", ""), New System.Data.SqlClient.SqlParameter("@Original_Jumlah", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Jumlah", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlDeleteCommand2
        '
        Me.SqlDeleteCommand2.CommandText = resources.GetString("SqlDeleteCommand2.CommandText")
        Me.SqlDeleteCommand2.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand2.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_Nomor", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nomor", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@IsNull_ID_Pemakaian", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, CType(0, Byte), CType(0, Byte), "ID Pemakaian", System.Data.DataRowVersion.Original, True, Nothing, "", "", ""), New System.Data.SqlClient.SqlParameter("@Original_ID_Pemakaian", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ID Pemakaian", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@IsNull_Jenis_Data_per_Bulan", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, CType(0, Byte), CType(0, Byte), "Jenis Data per Bulan", System.Data.DataRowVersion.Original, True, Nothing, "", "", ""), New System.Data.SqlClient.SqlParameter("@Original_Jenis_Data_per_Bulan", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Jenis Data per Bulan", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@IsNull_Periode", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, CType(0, Byte), CType(0, Byte), "Periode", System.Data.DataRowVersion.Original, True, Nothing, "", "", ""), New System.Data.SqlClient.SqlParameter("@Original_Periode", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Periode", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@IsNull_Tahun", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, CType(0, Byte), CType(0, Byte), "Tahun", System.Data.DataRowVersion.Original, True, Nothing, "", "", ""), New System.Data.SqlClient.SqlParameter("@Original_Tahun", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Tahun", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@IsNull_Jumlah", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, CType(0, Byte), CType(0, Byte), "Jumlah", System.Data.DataRowVersion.Original, True, Nothing, "", "", ""), New System.Data.SqlClient.SqlParameter("@Original_Jumlah", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Jumlah", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlDataAdapter2
        '
        Me.SqlDataAdapter2.DeleteCommand = Me.SqlDeleteCommand2
        Me.SqlDataAdapter2.InsertCommand = Me.SqlInsertCommand2
        Me.SqlDataAdapter2.SelectCommand = Me.SqlSelectCommand2
        Me.SqlDataAdapter2.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Periode", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Nomor", "Nomor"), New System.Data.Common.DataColumnMapping("ID Pemakaian", "ID Pemakaian"), New System.Data.Common.DataColumnMapping("Jenis Data per Bulan", "Jenis Data per Bulan"), New System.Data.Common.DataColumnMapping("Periode", "Periode"), New System.Data.Common.DataColumnMapping("Tahun", "Tahun"), New System.Data.Common.DataColumnMapping("Jumlah", "Jumlah")})})
        Me.SqlDataAdapter2.UpdateCommand = Me.SqlUpdateCommand2
        '
        'SqlSelectCommand3
        '
        Me.SqlSelectCommand3.CommandText = "SELECT        Nomor, IDPemakaian AS [ID Pemakaian], JenisPelanggan AS [Jenis Data" & _
    " per Pelanggan], KelompokPelanggan AS [Kelompok Pelanggan], Tahun, Jumlah" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "FROM " & _
    "           Pelanggan"
        Me.SqlSelectCommand3.Connection = Me.SqlConnection1
        '
        'SqlInsertCommand3
        '
        Me.SqlInsertCommand3.CommandText = resources.GetString("SqlInsertCommand3.CommandText")
        Me.SqlInsertCommand3.Connection = Me.SqlConnection1
        Me.SqlInsertCommand3.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Nomor", System.Data.SqlDbType.Int, 0, "Nomor"), New System.Data.SqlClient.SqlParameter("@ID_Pemakaian", System.Data.SqlDbType.Int, 0, "ID Pemakaian"), New System.Data.SqlClient.SqlParameter("@Jenis_Data_per_Pelanggan", System.Data.SqlDbType.VarChar, 0, "Jenis Data per Pelanggan"), New System.Data.SqlClient.SqlParameter("@Kelompok_Pelanggan", System.Data.SqlDbType.VarChar, 0, "Kelompok Pelanggan"), New System.Data.SqlClient.SqlParameter("@Tahun", System.Data.SqlDbType.VarChar, 0, "Tahun"), New System.Data.SqlClient.SqlParameter("@Jumlah", System.Data.SqlDbType.Float, 0, "Jumlah")})
        '
        'SqlUpdateCommand3
        '
        Me.SqlUpdateCommand3.CommandText = resources.GetString("SqlUpdateCommand3.CommandText")
        Me.SqlUpdateCommand3.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand3.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Nomor", System.Data.SqlDbType.Int, 0, "Nomor"), New System.Data.SqlClient.SqlParameter("@ID_Pemakaian", System.Data.SqlDbType.Int, 0, "ID Pemakaian"), New System.Data.SqlClient.SqlParameter("@Jenis_Data_per_Pelanggan", System.Data.SqlDbType.VarChar, 0, "Jenis Data per Pelanggan"), New System.Data.SqlClient.SqlParameter("@Kelompok_Pelanggan", System.Data.SqlDbType.VarChar, 0, "Kelompok Pelanggan"), New System.Data.SqlClient.SqlParameter("@Tahun", System.Data.SqlDbType.VarChar, 0, "Tahun"), New System.Data.SqlClient.SqlParameter("@Jumlah", System.Data.SqlDbType.Float, 0, "Jumlah"), New System.Data.SqlClient.SqlParameter("@Original_Nomor", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nomor", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@IsNull_ID_Pemakaian", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, CType(0, Byte), CType(0, Byte), "ID Pemakaian", System.Data.DataRowVersion.Original, True, Nothing, "", "", ""), New System.Data.SqlClient.SqlParameter("@Original_ID_Pemakaian", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ID Pemakaian", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@IsNull_Jenis_Data_per_Pelanggan", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, CType(0, Byte), CType(0, Byte), "Jenis Data per Pelanggan", System.Data.DataRowVersion.Original, True, Nothing, "", "", ""), New System.Data.SqlClient.SqlParameter("@Original_Jenis_Data_per_Pelanggan", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Jenis Data per Pelanggan", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@IsNull_Kelompok_Pelanggan", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, CType(0, Byte), CType(0, Byte), "Kelompok Pelanggan", System.Data.DataRowVersion.Original, True, Nothing, "", "", ""), New System.Data.SqlClient.SqlParameter("@Original_Kelompok_Pelanggan", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Kelompok Pelanggan", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@IsNull_Tahun", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, CType(0, Byte), CType(0, Byte), "Tahun", System.Data.DataRowVersion.Original, True, Nothing, "", "", ""), New System.Data.SqlClient.SqlParameter("@Original_Tahun", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Tahun", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@IsNull_Jumlah", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, CType(0, Byte), CType(0, Byte), "Jumlah", System.Data.DataRowVersion.Original, True, Nothing, "", "", ""), New System.Data.SqlClient.SqlParameter("@Original_Jumlah", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Jumlah", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlDeleteCommand3
        '
        Me.SqlDeleteCommand3.CommandText = resources.GetString("SqlDeleteCommand3.CommandText")
        Me.SqlDeleteCommand3.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand3.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_Nomor", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nomor", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@IsNull_ID_Pemakaian", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, CType(0, Byte), CType(0, Byte), "ID Pemakaian", System.Data.DataRowVersion.Original, True, Nothing, "", "", ""), New System.Data.SqlClient.SqlParameter("@Original_ID_Pemakaian", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ID Pemakaian", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@IsNull_Jenis_Data_per_Pelanggan", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, CType(0, Byte), CType(0, Byte), "Jenis Data per Pelanggan", System.Data.DataRowVersion.Original, True, Nothing, "", "", ""), New System.Data.SqlClient.SqlParameter("@Original_Jenis_Data_per_Pelanggan", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Jenis Data per Pelanggan", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@IsNull_Kelompok_Pelanggan", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, CType(0, Byte), CType(0, Byte), "Kelompok Pelanggan", System.Data.DataRowVersion.Original, True, Nothing, "", "", ""), New System.Data.SqlClient.SqlParameter("@Original_Kelompok_Pelanggan", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Kelompok Pelanggan", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@IsNull_Tahun", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, CType(0, Byte), CType(0, Byte), "Tahun", System.Data.DataRowVersion.Original, True, Nothing, "", "", ""), New System.Data.SqlClient.SqlParameter("@Original_Tahun", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Tahun", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@IsNull_Jumlah", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, CType(0, Byte), CType(0, Byte), "Jumlah", System.Data.DataRowVersion.Original, True, Nothing, "", "", ""), New System.Data.SqlClient.SqlParameter("@Original_Jumlah", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Jumlah", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlDataAdapter3
        '
        Me.SqlDataAdapter3.DeleteCommand = Me.SqlDeleteCommand3
        Me.SqlDataAdapter3.InsertCommand = Me.SqlInsertCommand3
        Me.SqlDataAdapter3.SelectCommand = Me.SqlSelectCommand3
        Me.SqlDataAdapter3.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Pelanggan", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Nomor", "Nomor"), New System.Data.Common.DataColumnMapping("ID Pemakaian", "ID Pemakaian"), New System.Data.Common.DataColumnMapping("Jenis Data per Pelanggan", "Jenis Data per Pelanggan"), New System.Data.Common.DataColumnMapping("Kelompok Pelanggan", "Kelompok Pelanggan"), New System.Data.Common.DataColumnMapping("Tahun", "Tahun"), New System.Data.Common.DataColumnMapping("Jumlah", "Jumlah")})})
        Me.SqlDataAdapter3.UpdateCommand = Me.SqlUpdateCommand3
        '
        'FormDataPemakaian
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(863, 566)
        Me.Controls.Add(Me.TabControl1)
        Me.MaximizeBox = False
        Me.Name = "FormDataPemakaian"
        Me.Text = "Data Pemakaian Energi Listrik"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsJenis1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.DsPeriode1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage4.PerformLayout()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage5.ResumeLayout(False)
        Me.TabPage5.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.DsPelanggan1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage6.ResumeLayout(False)
        Me.TabPage6.PerformLayout()
        CType(Me.DataGridView3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents Btn_Exit As System.Windows.Forms.Button
    Friend WithEvents Btn_Peramalan As System.Windows.Forms.Button
    Friend WithEvents Btn_Menu As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Txt_ID As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Btn_Simpan1 As System.Windows.Forms.Button
    Friend WithEvents Txt_Jenis As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents Btn_Refresh1 As System.Windows.Forms.Button
    Friend WithEvents Btn_Delete1 As System.Windows.Forms.Button
    Friend WithEvents Btn_Ubah1 As System.Windows.Forms.Button
    Friend WithEvents Btn_Cari1 As System.Windows.Forms.Button
    Friend WithEvents Txt_Cari As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Cbo_ID As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Cbo_Tahun As System.Windows.Forms.ComboBox
    Friend WithEvents Btn_Simpan2 As System.Windows.Forms.Button
    Friend WithEvents Cbo_Periode As System.Windows.Forms.ComboBox
    Friend WithEvents Txt_Jumlah As System.Windows.Forms.TextBox
    Friend WithEvents Cbo_Jenis As System.Windows.Forms.ComboBox
    Friend WithEvents Txt_No As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents Btn_Refresh2 As System.Windows.Forms.Button
    Friend WithEvents Btn_Delete2 As System.Windows.Forms.Button
    Friend WithEvents Btn_Ubah2 As System.Windows.Forms.Button
    Friend WithEvents Btn_Cari2 As System.Windows.Forms.Button
    Friend WithEvents Txt_Cari2 As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents TabPage5 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Cbo_ID2 As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Btn_Simpan3 As System.Windows.Forms.Button
    Friend WithEvents Cbo_Tahun2 As System.Windows.Forms.ComboBox
    Friend WithEvents Cbo_Kelompok As System.Windows.Forms.ComboBox
    Friend WithEvents Txt_Jumlah2 As System.Windows.Forms.TextBox
    Friend WithEvents Cbo_Jenis2 As System.Windows.Forms.ComboBox
    Friend WithEvents Txt_No2 As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents TabPage6 As System.Windows.Forms.TabPage
    Friend WithEvents Btn_Refresh3 As System.Windows.Forms.Button
    Friend WithEvents Btn_Delete3 As System.Windows.Forms.Button
    Friend WithEvents Btn_Ubah3 As System.Windows.Forms.Button
    Friend WithEvents Btn_Cari3 As System.Windows.Forms.Button
    Friend WithEvents Txt_Cari3 As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDataAdapter1 As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridView2 As System.Windows.Forms.DataGridView
    Friend WithEvents IDPemakaianDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents JenisDataDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridView3 As System.Windows.Forms.DataGridView
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDataAdapter2 As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents NomorDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDPemakaianDataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents JenisDataDataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PeriodeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TahunDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents JumlahDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDPemakaianDataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents JenisDataDataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents KelompokPelangganDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Txt_JenisPelanggan As System.Windows.Forms.TextBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents DsJenis1 As Forecast.DsJenis
    Friend WithEvents IDPemakaianDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents JenisDataPerBulanDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents JenisDataPerPelangganDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DsPeriode1 As Forecast.DsPeriode
    Friend WithEvents SqlSelectCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDataAdapter3 As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents DsPelanggan1 As Forecast.DsPelanggan
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDPemakaianDataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents JenisDataPerPelangganDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDPemakaianDataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents JenisDataPerBulanDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn


End Class
