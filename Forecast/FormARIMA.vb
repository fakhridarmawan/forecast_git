﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms.DataVisualization.Charting

Public Class FormARIMA

    Private _parsialautokorelasi As Double()

    Private Property parsialautokorelasi(ByVal k As Integer) As Double()
        Get
            Return _parsialautokorelasi
        End Get
        Set(ByVal value As Double())
            _parsialautokorelasi = value
        End Set
    End Property

    Private Sub Btn_Menu3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Menu3.Click
        FormMenuUtama.Show()
        Me.Hide()
    End Sub

    Private Sub Btn_Data2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Data2.Click
        FormDataPemakaian.Show()
        Me.Hide()
    End Sub

    Private Sub Btn_Exit3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Exit3.Click
        Me.Close()
    End Sub


    Function get_table(ByVal sql As String) As DataTable
        Dim da As New SqlDataAdapter
        da.SelectCommand.CommandText = sql
        Dim dt As New DataTable
        da.Fill(dt)
        Return dt
    End Function

    Private Sub Pasif()
        Cb_Periode.Enabled = False
        Cb_Pelanggan.Enabled = False
        Cb_Kelompok.Enabled = False
    End Sub

   
    Public Function CMonth(ByVal Periode As String) As String
        If (Periode = "1") Then
            Return "Januari"
        ElseIf (Periode = "2") Then
            Return "Pebruri"
        ElseIf (Periode = "3") Then
            Return "Maret"
        ElseIf (Periode = "4") Then
            Return "April"
        ElseIf (Periode = "5") Then
            Return "Mei"
        ElseIf (Periode = "6") Then
            Return "Juni"
        ElseIf (Periode = "7") Then
            Return "Juli"
        ElseIf (Periode = "8") Then
            Return "Augustus"
        ElseIf (Periode = "9") Then
            Return "September"
        ElseIf (Periode = "10") Then
            Return "Oktober"
        ElseIf (Periode = "11") Then
            Return "Nopember"
        ElseIf (Periode = "12") Then
            Return "Desember"
        Else
            Return Nothing
        End If
    End Function

    Private Sub Rb_Periode_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Rb_Periode.CheckedChanged

        If Rb_Periode.Checked = True Then

            DataGridView1.Visible = True
            Call readPeriode()
            'Call AktifPeriode()
            Cb_Periode.Enabled = True
            Cb_Pelanggan.Enabled = False
            Cb_Kelompok.Enabled = False
        End If

    End Sub

    Private Sub Rb_Pelanggan_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Rb_Pelanggan.CheckedChanged

        If Rb_Pelanggan.Checked = True Then

            DataGridView1.Visible = True
            DataGridView1.DataSource = DBNull.Value
            Cb_Pelanggan.Items.Clear()
            Call readPelanggan()

            'Call AktifPelanggan()
            Cb_Kelompok.Enabled = False
            Cb_Pelanggan.Enabled = True
            Cb_Periode.Enabled = False

        ElseIf Rb_Pelanggan.Checked = False Then
            Cb_Pelanggan.Items.Clear()
            Cb_Pelanggan.Refresh()

        End If

    End Sub

    Private Sub Rb_Kelompok_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Rb_Kelompok.CheckedChanged
        If Rb_Kelompok.Checked = True Then

            DataGridView1.Visible = True
            DataGridView1.DataSource = DBNull.Value
            Call readKelompok()
            Call readPelanggan()
            'Rb_Pelanggan.Checked = False
            Cb_Kelompok.Enabled = True
            Cb_Pelanggan.Enabled = True
            Cb_Periode.Enabled = False
        ElseIf Rb_Kelompok.Checked = False Then
            Cb_Kelompok.Items.Clear()
            Cb_Kelompok.Refresh()
        End If

    End Sub

    'Menampilkan Jenis Data Periode pada Combobox Data Periode

    Private Sub readPeriode()
        Try
            Dim SQL As String
            Dim Dr As SqlDataReader
            SqlConnection1.Open()
            SQL = "SELECT DISTINCT  JenisData FROM Periode"
            Dim cmd As New SqlCommand(SQL, SqlConnection1)
            Dr = cmd.ExecuteReader()
            While Dr.Read()
                Cb_Periode.Items.Add(Dr("JenisData"))
            End While
            'Cb_Periode.SelectedIndex = 1
        Catch ex As Exception
            'gagal memasukkan data
        Finally
            SqlConnection1.Close()
        End Try

    End Sub

    'Menampilkan Jenis Data Pelanggan pada Combobox Data Pelanggan

    Private Sub readPelanggan()
        Dim SQL As String
        Dim DrPelanggan As SqlDataReader
        SqlConnection1.Open()
        SQL = "SELECT DISTINCT JenisPelanggan FROM Pelanggan"
        Dim cmd As New SqlCommand(SQL, SqlConnection1)
        DrPelanggan = cmd.ExecuteReader()
        While DrPelanggan.Read()
            Cb_Pelanggan.Items.Add(DrPelanggan("JenisPelanggan"))
        End While
        'Cb_Pelanggan.SelectedIndex = 0
        SqlConnection1.Close()

    End Sub

    'Menampilkan Jenis Data Pelanggan berdasarkan Kelompok Pelanggan pada ComboBox Kelompok

    Private Sub readKelompok()
        Dim SQL As String
        Dim DrKelompok As SqlDataReader
        SqlConnection1.Open()
        SQL = "SELECT DISTINCT KelompokPelanggan AS [Kelompok Pelanggan] FROM Pelanggan"
        Dim cmd As New SqlCommand(SQL, SqlConnection1)
        DrKelompok = cmd.ExecuteReader()

        While DrKelompok.Read()
            Cb_Kelompok.Items.Add(DrKelompok("Kelompok Pelanggan"))
        End While


        'Cb_Kelompok.SelectedIndex = 0
        SqlConnection1.Close()


    End Sub

   

    'Menampilkan Data Periode Pada DataGridView

    Private Sub Cb_Periode_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cb_Periode.SelectedIndexChanged

        Dim strsql As String = "SELECT Periode, Tahun, Jumlah FROM Periode WHERE JenisData ='" & Cb_Periode.SelectedItem & "'"
        SqlConnection1.Open()
        Dim objcmd As New SqlCommand()
        objcmd.CommandText = strsql
        objcmd.CommandType = CommandType.Text
        objcmd.Connection = SqlConnection1

        Dim ForecastDa As New SqlDataAdapter
        ForecastDa.SelectCommand = objcmd
        Dim ForecastDs As New DataSet
        Dim ForecastDt As New DataTable

        ForecastDa.Fill(ForecastDs, "Periode")
        ForecastDt = ForecastDs.Tables("Periode")

        SqlConnection1.Close()

        DataGridView1.AutoGenerateColumns = True
        DataGridView1.DataSource = ForecastDt

        Dim ForecastDr As DataRow
        ForecastDr = ForecastDt.NewRow()
        ForecastDr("Periode") = (ForecastDt.Rows(0).Item("Periode"))
        ForecastDr("Tahun") = ForecastDt.Rows(0).Item("Tahun")
        ForecastDr("Jumlah") = CDbl(ForecastDt.Rows(0).Item("Jumlah"))

        'ForecastDt.Rows.Add(ForecastDr)
        DataGridView1.Columns("Jumlah").DefaultCellStyle.Format = "N2"

        Try
            SqlConnection1.Open()
            Dim reader As SqlDataReader = objcmd.ExecuteReader
            'While reader.Read()
            reader.Read()
            DataGridView1.Rows(0).Cells(0).Value = reader("Periode").ToString
            DataGridView1.Rows(0).Cells(1).Value = reader("Tahun").ToString
            DataGridView1.Rows(0).Cells(2).Value = reader("Jumlah").ToString
            'End While
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            SqlConnection1.Close()
        End Try

    End Sub

    'Menampilkan Data Pelanggan pada DataGridView

    Private Sub Cb_Pelanggan_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cb_Pelanggan.SelectedIndexChanged



        Dim sSql As String = "SELECT KelompokPelanggan AS [Kelompok Pelanggan], Tahun AS [Periode], Jumlah FROM Pelanggan WHERE JenisPelanggan ='" & Cb_Pelanggan.SelectedItem & "'"
        SqlConnection1.Open()
        Dim command As New SqlCommand()
        command.CommandText = sSql
        command.CommandType = CommandType.Text
        command.Connection = SqlConnection1

        Dim ForecastDa2 As New SqlDataAdapter
        ForecastDa2.SelectCommand = command
        Dim ForecastDs2 As New DataSet
        Dim ForecastDt2 As New DataTable

        ForecastDa2.Fill(ForecastDs2, "Pelanggan")
        ForecastDt2 = ForecastDs2.Tables("Pelanggan")

        SqlConnection1.Close()

        DataGridView1.AutoGenerateColumns = True
        DataGridView1.DataSource = ForecastDt2

        
        Dim ForecastDr2 As DataRow
        ForecastDr2 = ForecastDt2.NewRow()
        ForecastDr2("Kelompok Pelanggan") = ForecastDt2.Rows(0).Item("Kelompok Pelanggan")
        ForecastDr2("Periode") = CInt(ForecastDt2.Rows(0).Item("Periode"))
        ForecastDr2("Jumlah") = CDbl(ForecastDt2.Rows(0).Item("Jumlah"))

        'ForecastDt.Rows.Add(ForecastDr)
        DataGridView1.Columns("Jumlah").DefaultCellStyle.Format = "N2"


        Try
            SqlConnection1.Open()
            Dim baca As SqlDataReader = command.ExecuteReader
            'While reader.Read()
            baca.Read()
            DataGridView1.Rows(0).Cells(0).Value = baca("Kelompok Pelanggan").ToString
            DataGridView1.Rows(0).Cells(1).Value = baca("Periode").ToString
            DataGridView1.Rows(0).Cells(2).Value = baca("Jumlah").ToString
            'End While

            DataGridView1.Sort(DataGridView1.Columns(0), System.ComponentModel.ListSortDirection.Ascending)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            SqlConnection1.Close()
        End Try


    End Sub

    'Menampilkan Data Kelompok dan JenisPelanggan Pada DataGridView

    Private Sub Cb_Kelompok_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cb_Kelompok.SelectedIndexChanged
        Dim JenisPelanggan = Cb_Pelanggan.SelectedItem
        Dim sSql As String = "SELECT KelompokPelanggan AS [Kelompok Pelanggan], Tahun AS [Periode], Jumlah FROM Pelanggan WHERE JenisPelanggan= '" & JenisPelanggan & "' AND KelompokPelanggan= '" & Cb_Kelompok.SelectedItem & "' "
        SqlConnection1.Open()
        Dim command As New SqlCommand()
        command.CommandText = sSql
        command.CommandType = CommandType.Text
        command.Connection = SqlConnection1

        Dim ForecastDa3 As New SqlDataAdapter
        ForecastDa3.SelectCommand = command
        Dim ForecastDs3 As New DataSet
        Dim ForecastDt3 As New DataTable

        ForecastDa3.Fill(ForecastDs3, "Pelanggan")

        ForecastDt3 = ForecastDs3.Tables("Pelanggan")

        SqlConnection1.Close()

        DataGridView1.AutoGenerateColumns = True
        DataGridView1.DataSource = ForecastDt3


        'Dim ForecastDr3 As DataRow
        'ForecastDr3 = ForecastDt3.NewRow()
        'ForecastDr3("KelompokPelanggan") = ForecastDt3.Rows(0).Item("KelompokPelanggan")
        'ForecastDr3("Tahun") = ForecastDt3.Rows(0).Item("Tahun")
        'ForecastDr3("Jumlah") = CDbl(ForecastDt3.Rows(0).Item("Jumlah"))

        'ForecastDt.Rows.Add(ForecastDr)
        DataGridView1.Columns("Jumlah").DefaultCellStyle.Format = "N2"


        Try
            SqlConnection1.Open()
            'Dim command As New SqlCommand("SELECT KelompokPelanggan, Tahun, Jumlah FROM Pelanggan WHERE KelompokPelanggan = '" & Cb_Kelompok.SelectedItem & "'", SqlConnection1)
            Dim baca As SqlDataReader = command.ExecuteReader
            'While baca.Read()
            baca.Read()
            DataGridView1.Rows(0).Cells(0).Value = baca("Kelompok Pelanggan").ToString
            DataGridView1.Rows(0).Cells(1).Value = baca("Periode").ToString
            DataGridView1.Rows(0).Cells(2).Value = baca("Jumlah").ToString
            'End While
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Silahkan Pilih Jenis Data Pelanggan Terlebih Dahulu!!")
        Finally
            SqlConnection1.Close()
        End Try

    End Sub

    Private Sub Btn_Submit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_Submit.Click

        'Hitung Rata - Rata

        Dim Rata As Double = 0
        Dim Total As Double
        Dim i As Integer
        'Dim X As Double
        'Dim d = DataGridView1.RowCount
        With DataGridView1
            'X = DataGridView1.RowCount - 1
            For i = 0 To DataGridView1.RowCount - 1
                If DataGridView1.Rows(0).Cells(1).Value Then
                    Total += CDbl(DataGridView1.Rows(i).Cells("Jumlah").Value)
                End If
            Next
        End With
        Rata = Total / DataGridView1.RowCount

        'Hitung Identifikasi Model dengan menggunakan proses ACF/Autocorelation

        Dim j As Integer
        Dim b = DataGridView1.Rows(1).Cells("Jumlah").Value
        'Dim data1 = DataGridView1.Rows(i).Cells("Jumlah").Value
        Dim periode = DataGridView1.RowCount
        Dim data1(periode), TAC(periode), sacf(periode), acf(periode), temp, temp1, temp2, t(periode), t1(periode) As Double

        For i = 1 To periode
            'j = i + 1
            data1(i) = DataGridView1.Rows(i - 1).Cells("Jumlah").Value
        Next


        For i = 1 To periode
            temp = 0
            temp1 = 0
            For j = 1 To (periode - i)
                'For j = 0 To (periode - i - 1)
                'data1(j) = DataGridView1.Rows(j).Cells("Jumlah").Value
                temp = temp + (data1(j) - Rata) * (data1(j + i) - Rata)
            Next
            t(i) = temp
            For j = 1 To periode
                'data1(j) = DataGridView1.Rows(j).Cells("Jumlah").Value
                temp1 = temp1 + ((data1(j) - Rata) * (data1(j) - Rata))
            Next
            t1(i) = temp1
            acf(i) = temp / temp1

        Next

        Dim z As Double
        z = 1 / 2

        For i = 1 To periode
            temp = 0
            If i = 0 Then
                sacf(i) = 1 / Math.Pow((periode + 1 - b), z)
            Else
                temp2 = 0
                For j = 0 To i - 1
                    temp2 = temp2 + Math.Pow(acf(j), 2)

                Next
                sacf(i) = Math.Pow(1 + (2 * temp2), z) / Math.Pow((periode + 1), z)
            End If
            TAC(i) = acf(i) / sacf(i)
        Next

        Dim x As Integer
        DataGridView2.EditMode = False
        DataGridView2.ColumnCount = 3
        DataGridView2.RowCount = (periode + 1)
        DataGridView2.Item(0, 0).Value = "Periode"
        DataGridView2.Item(1, 0).Value = "ACF"
        DataGridView2.Item(2, 0).Value = "TAC"
        For x = 1 To periode
            DataGridView2.Item(0, x).Value = x
            DataGridView2.Item(1, x).Value = acf(x)
            DataGridView2.Item(2, x).Value = TAC(x)
        Next
        DataGridView2.Columns(1).DefaultCellStyle.Format = "N5"
        DataGridView2.Columns(2).DefaultCellStyle.Format = "N5"


        Chart1.DataSource = DataGridView2
        Chart1.Series.Clear()
        'Chart1.Titles.Add("GRAFIK ACF")
        Chart1.Visible = True

        Dim s As New Series
        s.ChartType = SeriesChartType.BoxPlot
        'Dim NilaiACF As Integer = DataGridView2.Columns(1).ToString
        For x = 1 To periode
            s.Points.AddXY(x, acf(x))
        Next

        Chart1.Series.Add(s)

        'Proses pengecekan kestasioneran data

        Dim cek As Integer
        Dim interval As Double
        Dim CekStasioner As Boolean
        'Dim z As Double

        If periode - 1 = 0 Then
            interval = 1.96
        Else
            interval = 1.96 * (1 / Math.Sqrt(periode - 1)) 'jumlah kepercayaan 95% Z 0.025 = 1.96
        End If
        z = 0 - interval
        cek = True
        For x = 1 To periode
            If (TAC(x - 1) > interval) Or (TAC(x - 1) < z) Then
                cek = False
            End If
        Next

        If cek = True Then
            CekStasioner = False
            'MessageBox.Show("Data Belum Stasioner", "Masuk Ke Proses Differencing", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Else
            CekStasioner = True
            'MessageBox.Show("Data Stasioner", "Data Tidak Perlu Proses Differencing", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If

        'Differencing model untuk menstasioner nilai jika nilai belum stasioner

        Dim data(periode) As Double
        For x = 1 To periode - 1
            data(x) = data(x) - data(x - 1)
        Next
        temp = 0
        For x = 1 To periode - 2
            temp = temp + data(x)
        Next
        Rata = temp / periode - 1
        'MessageBox.Show("Data Sudah Berhasil di Differencing", "Lanjutkan Tahap", MessageBoxButtons.OK, MessageBoxIcon.Information)


        'Hitung Identifikasi Model PACF (Partial Autocorelation) setelah pengecekan stasioner dan proses Autocorelation (ACF)

        Dim k, l As Integer
        Dim parsial As Double() = New Double(periode) {}
        Dim korelasi As Double() = New Double(periode) {}
        'Dim parsialautokorelasi As Double()() = New Double(periode)() {}
        Dim pacf(periode), spacf(periode), TPAC(periode) As Double

        temp1 = 0
        temp2 = 0
        For k = 1 To 200

            For l = 1 To k
                parsialautokorelasi(k) = New Double(periode) {}

            Next
        Next
        pacf(0) = acf(0)
        parsialautokorelasi(0)(0) = acf(0)
        For k = 1 To periode
            For l = 1 To periode
                parsialautokorelasi(k)(l) = 0
            Next
            parsial(k) = 0
            korelasi(k) = 0
        Next

        pacf(0) = acf(0)
        parsialautokorelasi(0)(0) = acf(0)
        For x = 1 To periode
            k = x
            temp1 = 0
            temp2 = 0
            For l = 1 To x - 1
                temp1 = temp1 + (parsialautokorelasi(x - 1)(l) * acf(x - l - 1))
                temp2 = temp2 + (parsialautokorelasi(x - 1)(l) * acf(1))
            Next
            parsialautokorelasi(x)(k) = (acf(x) - temp1) / (l - temp2)
            k = k - 1
            While k >= 0
                parsialautokorelasi(x)(k) = parsialautokorelasi(x - 1)(k) - (parsialautokorelasi(x)(x) * parsialautokorelasi(x - 1)(k))
                k = k - 1
            End While
        Next

        For j = 1 To periode
            pacf(j) = parsialautokorelasi(j)(j)
        Next
        For j = 1 To periode
            spacf(j) = 1 / Math.Pow((periode + 1), 1 / 2)
            TPAC(j) = pacf(j) / spacf(j)
        Next

        DataGridView3.ColumnCount = 2
        DataGridView3.RowCount = (periode + 1)

        DataGridView3.Item(0, 0).Value = "LAG"
        DataGridView3.Item(1, 0).Value = "Data"

        For x = 1 To periode
            DataGridView3.Item(0, x).Value = x
            DataGridView3.Item(1, x).Value = pacf(x)
        Next

        DataGridView3.Columns(1).DefaultCellStyle.Format = "N5"

        Chart2.DataSource = DataGridView3
        Chart2.Series.Clear()
        'Chart2.Titles.Add("GRAFIK ACF")
        Chart2.Visible = True

        Dim sS As New Series
        sS.ChartType = SeriesChartType.BoxPlot
        'Dim NilaiACF As Integer = DataGridView2.Columns(1).ToString
        For x = 1 To periode
            sS.Points.AddXY(x, pacf(x))
        Next

        Chart2.Series.Add(sS)

    End Sub

    Private Sub FormARIMA_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


        DataGridView1.Visible = False
        'Call AddPeriodeColumns()
        Call Pasif()

    End Sub

End Class